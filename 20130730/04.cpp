// Explique el propósito de la función pthread_join. Ejemplifique.

// Su proposito es el de esperar a que otro thread termine.
// ejemplo.


// ejemplo en c++
// El thread principal necesita esperar a t a que calcule el total, si no
// se realizara el join() se imprimiria un valor invalido.
#include <thread>
#include <iostream>
#include <vector>

void f(std::vector<int> &v, int &total) {
    for (auto e: v) {
        total += e;
    }
}

int main() {
    std::vector<int> v{1, 2, 3, 4, 5};
    int total = 0;

    std::thread t([&]{f(v, total);});
    t.join();

    std::cout << total << std::endl;

    return 0;
}
