// ¿Qué es la herencia?¿Para qué se utiliza?¿Qué tipos conoce?

// La herencia es un mecanismo en el cual una clase hereda, comparte
// comportamiento de otra y es comun que se forme entre ellas la relacion
// "es un", por ejemplo, si se quiere modelar mediante clases a un grupo
// de perros y gatos se podria utilizar una clase Animal y derivar dos
// de ellas Perro y Gato que compartan algunos metodos y atributos, especialmente
// en C++ se heredan atributos, metodos pero no constructores, friends ni
// operadores.

// Es utilizada para encapsular comportamiento en comun entre clases y asi
// evitar la duplicacion de codigo y hacer que sea mas facilmente manejable.

// Los tipos son simple, en el que una clase solo deriva de una clase o
// multiple en que una clase deriva de varias otras mas.
