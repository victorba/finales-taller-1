// Escriba un programa ISO C que procese el archivo de números
// datos.txt sobre sí mismo. El procesamiento consite en convertir
// los números encontrados (de 1 o más cifras decimales) a octal.

// Asumo que son solo numeros enteros
// Asumo que un numero decimal a octal como mucho usara un caracter mas.
// Asumo que son positivos -> si son negativos, primero se les saca el signo.

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>

#define PATHNAME "datos.txt"

#define MAXBUFFER 10

void dec_to_oct(char *dest, char *src) {
    int n;
    int r;
    int i;
    char digit[2];

    i = strlen(src) - 1;
    dest[i + 1] = '\0';
    n = atoi(src);

    while (i >= 0) {
        r = n % 8;
        snprintf(digit, 2, "%d", r);
        dest[i] = digit[0];
        n = n / 8;
        i--;
    }
}


int main() {
    FILE *file;
    char src[MAXBUFFER];
    char dest[MAXBUFFER];
    int original_size;
    int new_size;
    int iread;
    int iwrite;
    char caracter;
    int cant_numeros = -1;

    if ((file = fopen(PATHNAME, "r+")) == NULL) {
        return -1;
    }

    while (fgets(src, MAXBUFFER, file) != NULL) {
        cant_numeros++;
    }

    original_size = ftell(file);
    iread = original_size;
    new_size = original_size + cant_numeros;
    fflush(file);
    ftruncate(fileno(file), new_size);

    fseek(file, 0, SEEK_END);
    iwrite = ftell(file);
    rewind(file);

    iread -= 2;
    while (iread >= 0) {
        fseek(file, iread, SEEK_SET);
        if (iread != 0) {
            fread(&caracter, sizeof(caracter), 1, file);
        }
        if (iread == 0 || caracter == '\n') {
            memset(src, '\0', MAXBUFFER);
            memset(dest, '\0', MAXBUFFER);
            fgets(src, MAXBUFFER, file);
            dec_to_oct(dest, src);
            dest[strlen(src)] = '\n';
            dest[strlen(src) + 1] = '\0';
            iwrite -= strlen(src);
            fseek(file, iwrite, SEEK_SET);
            fwrite(dest, strlen(src) + 1, 1, file);
            iwrite--;
        }
        iread--;
    }

    fclose(file);

    return 0;
}
