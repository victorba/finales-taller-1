// Declare la clase Número para almacenar un número de 100
// cifras decimales. Incluya: constructor default, constructor
// de copia, los operadores +, ++ (posfijo), ++ (prefijo), >, =,
// << (corrimiento de bits) y <<(impresión).
// Implemente el operador <<(impresión).
// Detalle

#include <cmath>
#include <iostream>
#include <iomanip>

#define MAX 3

class Numero {
private:
    double n;

public:
    Numero(double n): n(n) {}

    Numero(): n(0) {}

    Numero &operator=(const Numero &otro) {
        this->n = otro.n;
        return *this;
    }

    Numero(const Numero &otro) {
        this->n = otro.n;
    }

    Numero operator+(const Numero &otro) {
        double r = this->n + otro.n;
        r = r * std::pow(10, MAX);
        r = std::floor(r);
        r = r / std::pow(10, MAX);
        Numero res(this->n + otro.n);
        return res;
    }

    Numero &operator++() {
        this->n = this->n + std::pow(10, -MAX);
        return *this;
    }

    Numero operator++(int) {
        ++(*this);
        return *this;
    }

    friend std::ostream &operator<<(std::ostream &out, const Numero &numero) {
        out << std::fixed <<  std::setprecision(MAX) << numero.n;
        return out;
    }

    Numero operator<<(const int &otro) {
        int x = this->n * std::pow(10, MAX);
        x = x << otro;
        this->n = x / std::pow(10, MAX);
        return *this;
    }

    bool operator>(const Numero &otro) {
        return this->n > otro.n;
    }

};


int main() {
    Numero n(0.23);
    ++n;
    std::cout << n << std::endl;

    Numero x(0.1);
    if (n > x) {
        std::cout << "n es mayor";
    } else {
        std::cout << "x es mayor";
    }
    std::cout << std::endl;

    n = n << 2;
    std::cout << n << std::endl;


    return 0;
}
