#include "buffered_input_stream.h"

int BIS_init(BIS_t *bis, socket_t *skt, int MAX_SIZE) {
    char *r;

    r = (char *)malloc(MAX_SIZE);
    if (r == NULL) {
        return -1;
    }

    bis->buffer = r;
    bis->MAX_SIZE = MAX_SIZE;
    bis->skt = skt;
    bis->i = MAX_SIZE;

    return 0;
}

int BIS_read(BIS_t *bis, char *res) {
    int r;

    if (BIS_buffer_is_empty(bis)) {
        if ((r = BIS_refill_buffer(bis)) == -1) {
            return -1;
        }
    }

    *res = bis->buffer[bis->i];
    bis->i++;

    return 0;
}

bool BIS_buffer_is_empty(BIS_t *bis) {
    return bis->i == bis->MAX_SIZE;
}

int BIS_refill_buffer(BIS_t *bis) {
    int r;

    if ((r = socket_recv(bis->skt, bis->buffer, bis->MAX_SIZE, 0)) > 0) {
        bis->i = 0;
        return 0;
    } else {
        return -1;
    }
}
