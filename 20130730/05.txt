¿En qué consiste el proceso de precompilación?

El proceso de procompilacion consiste en
1. control y sustitucion de precompilacion
2. expansion de macros

1. Control y sustitucion de precompilacion
Consiste en incluir ciertos bloques de codigo dentro delcodigo fuente mediante
las siguientes directivas
    - #define CONSTANTE valor
      Se reemplaza con 'valor' en cada lugar donde aparezca CONSTANTE

    - #include <archivo>
      Se agrega al archivo el documento, por default busca en la carpeta
      por default del compilador o mediante "" busca segun una ruta dada.

    - #ifdef, #ifndef, #undef #endif
      Permite agregar codigo segun condiciones.

2. Expansion de macros
Reemplaza codigo y reciben parametros.
Ejemplo:
#define MIN(X, Y) ((X) < (Y) ? (X) : (Y))

Existen algunos operadores como
# stringficacion que toma a un parametro y lo reemplaza como una cadena
ejemplo. #define STR(X) #X --> reemplaza en codigo por "X" , el contenido de X.

## Concatenacion
#define CONCAT(X, Y) X##Y --> reemplaza el codigo por XY
