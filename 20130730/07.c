// ¿Qué objeto se provee en ISO C para manejar la
// consola de entrada?Ejemplifique.

// Para manejar la entrada por consola se dispone de un archivos estandar
// llamado stdin. Algunas funciones que tiene asociada son

int main() {
    int x;
    // Recibe un entero por entrada y almacena su valor en x.
    scanf("%d", &x);

    // Lee hasta 200 bytes de stdin y los almacena en buffer.
    char buffer[200];
    fread(buffer, sizeof(buffer), 1, stdin);

    // Lee un caracter de stdin
    char c = fgetc(stdint);

    return 0;
}
