// La declaración de una función denominada suma que tome como
//  parámetros 2 punteros a entero y devuelva un puntero a un número
//  de punto flotante de doble precisión.
//
double *suma(int *x, int *y);

// La definición de un puntero a una función que toma como
// parámetros un entero corto con signo y un puntero a puntero
// a caracter y devuelve un número de punto flotante. El mismo
// debe poder accederse desde cualquier módulo del programa.

float (*f)(short x, char *y);

// La definición de un entero con signo denominado A.
int A;

int main() {
    return 0;
}
