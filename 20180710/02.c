// Describa con exactitud las siguientes declaraciones/definiciones globales:
//
// extern char (*l)[3];
// Es una declaracion de un array l de punteros a char de longitud 3.
// Se almacena en el data segment.
//
// static float *C[3];
// Es una definicion de un array f de punteros a float de longitud 3.
// Al ser global puede ser accedida unicamente por las funciones dentro
// del mismo archivo. Se almacena en el data segment.
//
// static int F(const short *a);
// Declaracion de una funcion F que devuelve un entero y por parametro recibe
// un puntero a short constante (no puede  modificar el contenido de a).
// Al ser estatica y global solo puede ser accedida por las funciones del
// archivo.

extern char (*l)[3];
static float *C[3];
static int F(const short *a);

int F(const short *a) {
    // Si modifico a
    // *a = 2; => ERROR 'a' es solo de lectura
    return 0;
}

int main() {
    char a = 'a';
    char *l[3] = {&a, &a, &a};
    char *x = l[2];
    x++;

    *C[2] = 0.5;

    short s = 2;
    F(&s);
    return 0;
}
