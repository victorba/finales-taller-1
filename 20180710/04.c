// Defina una rutina en C que se conecte a la IP 8.8.8.8, al puerto 8192 y
// reciba un archivo binario. El archivo recibido debe ser descargado a un
// archivo llamado recibido.bin cuidando el uso de recursos de memoria. El
// archivo finaliza al detectar conexion cerrada.

#include "socket.h"
#include <stdbool.h>

#define NODE "localhost"
#define SERVICE "4546"
#define MAX_SVBUFFER 10
#define MAX_CLIBUFFER 5
#define PATHNAME "04-file.dat"
#define OUTFILE "recibido.bin"

typedef struct __socket0_t {
    int sockfd;
} socket0_t;

int socket0_client_init(socket0_t *skt, char node[], char service[]);

int socket0_recv(socket0_t *skt, char buf[], int size, int flags);

int socket0_close(socket0_t *skt);

void test();

int main(int argc, char *argv[]) {
    if (argc > 1) {
        test();
        return 0;
    }

    socket0_t cli;
    char clibuffer[MAX_CLIBUFFER + 1];
    clibuffer[MAX_CLIBUFFER] = '\0';
    FILE *file;
    int r;

    socket0_client_init(&cli, NODE, SERVICE);
    file = fopen(OUTFILE, "w+");

    while ((r = socket0_recv(&cli, clibuffer, MAX_CLIBUFFER, 0)) > 0) {
        printf("clibuffer: %s\n", clibuffer);
        fwrite(clibuffer, r, 1, file);
    }

    socket0_close(&cli);

    return 0;
}

int socket0_client_init(socket0_t *skt, char node[], char service[]) {
    struct addrinfo hints;
    struct addrinfo *res;
    struct addrinfo *ptr;
    int r;
    int sockfd;
    bool are_we_connected = false;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = 0;

    if ((r = getaddrinfo(node, service, &hints, &res)) == -1) {
        fprintf(stderr, "Error getaddrinfo: %s\n", gai_strerror(r));
        return -1;
    }

    for (ptr = res; ptr != NULL && !are_we_connected; ptr = ptr->ai_next) {
        sockfd = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
        if (sockfd != -1) {
            if ((r = connect(sockfd, ptr->ai_addr, ptr->ai_addrlen)) != -1) {
                are_we_connected = true;
            }
        }
    }

    freeaddrinfo(res);

    if (!are_we_connected) {
        return -1;
    }

    skt->sockfd = sockfd;
    return 0;
}

int socket0_recv(socket0_t *skt, char buf[], int size, int flags) {
    int r;
    if ((r = recv(skt->sockfd, buf, size, flags)) == -1) {
        perror("Error recv: ");
    }
    return r;
}

int socket0_close(socket0_t *skt) {
    int r;
    if ((r = close(skt->sockfd)) == -1) {
        perror("Error close: ");
    }
    return r;
}

void test() {
    socket_t sv;
    socket_t peer;
    FILE *file;
    char svbuffer[MAX_SVBUFFER];

    socket_server_init(&sv, SERVICE);
    socket_listen(&sv, 100);
    socket_accept(&sv, &peer);

    file = fopen(PATHNAME, "r");
    while (fread(svbuffer, MAX_SVBUFFER, 1, file) == 1) {
        socket_send_all(&peer, svbuffer, MAX_SVBUFFER);
    }

    socket_shutdown(&peer, SHUT_WR);
    socket_close(&peer);
    socket_close(&sv);
}
