// Explique la diferencia entre las etapas de compilacion y
// enlazado (linking). Escriba un breve ejemplo de codigo con
// errores para cada una de ellas indicandolos de forma clara
// Detalle

// Las etapas de compilacio y linking se diferencian sus archivos de entrada
// y de salida y los procesos que realizan sobre ellos.
// En la compilacion se recibe un archivo que ya pasó por la etapa de
// precompilacion y debe realizarse sobre el una etapa de parsing (armado
// de un arbol de simbolos), traduccion (pasaje de los simbolos a lengueje
// ensamblador) y por ultimo assembler (pasar de leng. ensamblador a codigo
// objeto).
//
// Por su parte, linking recibe a varios codigos objetos y debe convertirlos
// en un ejecutable resolviendo los simbolos faltantes en el codigo (llamado
// a funciones o variables)
// - Estatico: Se resuelven los simbolos antes del tiempo de ejecucion.
//             En un unico ejecutable se tiene a todo el codigo necesario
//             para su ejecucion
// - Dinamico: En tiempo de ejecucion se resuelven los simbolos faltantes.

// Ejemplo.


extern bool continuar_externo;
int person_print(persona_t *p);

int main() {
    bool continuar = true;
    // Error en tiempo de compilacion.
    // No se puede generar el arbol de simbolos.
    if (continuar) { ;

    // Error de linking.
    // No se puede resolver continuar_externo dado que no se brinda una
    // biblioteca en la que este definida.
    if (continuar_externo) {
        return 0;
    }

    // Error de compilacion.
    // persona_t no es definido y por lo tanto no sabe cuando espacio ocupa
    persona_t p;

    // Error de linking
    // Se sabe el tamanio de un puntero, pero no se tiene la definicion
    // de la funcion persona_print(persona_t *p)
    person_print(&p);

    return 0;
}
