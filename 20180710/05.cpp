// Declare la clase Oraciones que almacena una acumulacion de lineas de texto
// (strings) en cierto orden e incluye las operaciones de agregacion y
//  eliminacion de lineas.
//
// La clase debe poseer los operadores usuales de copia, asignacion, salida
// a flujo (<<) comparacion (==), agregacion (+), substraccion (-).
//
// Los 2 ultimos operadores deben admitir argumentos del tipo string en
// formato C (char*) y C++ (std::string).
// Detalle

#include <iostream>
#include <set>

class Oraciones {
private:
    std::multiset<std::string> lineas;

public:
    Oraciones() {}

    // Constructor por copia
    Oraciones(const Oraciones &otro) {
        this->lineas = otro.lineas;
    }

    // Constructor por asignacion por copia
    Oraciones &operator=(const Oraciones &otro) {
        this->lineas = otro.lineas;
        return *this;
    }

    friend std::ostream& operator<< (std::ostream &out, const Oraciones &o) {
        for (const std::string &linea: o.lineas) {
            out << linea << "\n";
        }
        return out;
    }

    bool operator==(const Oraciones &otro) {
        if (this->lineas.size() != otro.lineas.size()) {
            return false;
        }

        for (const std::string &linea: this->lineas) {
            if (otro.lineas.count(linea) == 0) {
                return false;
            }
        }

        for (const std::string &linea: otro.lineas) {
            if (this->lineas.count(linea) == 0) {
                return false;
            }
        }

        return true;
    }

    Oraciones operator+(const std::string &linea) {
        this->lineas.insert(linea);
        return *this;
    }

    Oraciones operator+(const char c_string[]) {
        std::string s(c_string);
        (*this) + s;
        return *this;
    }

    Oraciones operator-(const std::string &linea) {
        this->lineas.erase(linea);
        return *this;
    }

    Oraciones operator-(const char *c_string) {
        std::string s(c_string);
        (*this) - s;
        return *this;
    }


};

int main() {
    Oraciones o;
    char texto[] = "mundo";
    o = o + "hola";
    o = o + texto;
    std::cout << o;

    o = o - "hola";
    std::cout << o;

    Oraciones otro(o);
    std::cout << "otro: " << otro;

    Oraciones otro_mas;
    otro_mas = otro;
    std::cout << "otro_mas: " << otro_mas;

    return 0;
}
