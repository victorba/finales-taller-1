// Que es un thread? Que funciones se utilizan para
// manipularlos (lanzarlos, etc)?
// Detalle

// Un thread es una secuencia de instrucciones que representa una tarea
// separada y calendarizable por un scheduler.
// Los threads tienen estados que pueden ser inicio, ready, running,
// waiting y finished.
// Los threads comparten codigo, variables globales y heap, pero cada uno de
// ellos tiene su propio stack, thread metadata y valor de sus registros.

// Las funciones se presentan en el ejemplo.
// Otras funciones son
// - yield() que permiten ceder tiempo de procesador y
// - exit que terminan la ejecucion de un thread
// - detach que permite continuar con un thread independiente de otro.

#include <iostream>
#include <thread>

void print_n() {
    int n = 0;
    n++;
    std::cout << n << std::endl;
}

int main() {
    // Funcion de instantacion de un thread. El scheduler define cuando
    // ejecutarlo. Recibe por parametro una funcion y los parametros de ella.
    std::thread t(print_n);

    // Espera a que termine el thread.
    t.join();

    return 0;
}
