// Explique que es y para que sirve una variable de clase (o atributo estatico)
// en C++. Mediante un ejemplo de uso, indique como se define dicha variable, su
// inicializacion y el acceso a su valor para realizar una impresion simple
// dentro de un main.
// Detalle

// Las variables de clase son variables compartidas por todos las instancias
// de la clase y son utiles cuando se necesita compartir informacion entre
// todas ellas.
// Se inicializan al inicio del programa y no necesitan de una instancia, luego
// son liberadas al final de este.
// Para acceder a ellas dentro de una clase se hace tal como con cualquier
// otra variable, es decir, mediante this->static_variable.
// De ser publicas, se puede acceder mediante Class::static_variable.
// Ademas, son declaradas como un atributo mas en la declaracion de clase, pero
// su definicion se hace aparte mediante "type Class::static_variable = value"

// Ejemplo: Se tiene una clase Person y un atributo estatico counter que
//          cuenta los objetos instanciados. 

#include <iostream>

class Person {
private:
    int age;
    static int private_counter;
public:
    static int counter;
    Person(int age): age(age) {
        this->counter++;
    }
};

int Person::counter = 0;

int main() {
    for (int i = 0; i < 10; i++) {
        Person p(25);
    }

    std::cout << "Person counter: " << Person::counter << std::endl;
}
