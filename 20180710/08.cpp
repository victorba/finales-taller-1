// Defina el concepto de Mutex y de un ejemplo de uso.
// Indique en que casos es necesario.
// Detalle

// Un mutex es un objeto (Mutual Exclusion) que permite que permite a
// distintos thread compartir un recurso, pero no de forma simultanea sino
// que de uno a la vez.
//
// Son necesarios para evitar Race Conditions, es decir, problemas en los que
// se accede a un recurso compartido por mas de un thread a la vez para
// realizar operaciones de lecto/escritura y que dependiendo del orden o tiempo
// de los threads puedan dejar al recurso en un estado invalido.
//
// Ejemplo: Un contador
// Si no tuviera el mutex, entonces al haber dos threads intentando
// incrementar el valor n del counter se podria producir que
// t1 leyera 0, t2 leyera 0, t2 incrementa su valor a 1, t2 escribe 1,
// t1 incrementa su valor a 1, t1 ecribiera 1.
// con lo cual el contador tendria un estado invalido dado que se esperaba
// que tuviera un valor de 2.

#include <mutex>
#include <thread>
#include <iostream>

class Counter {
private:
    int n;
    std::mutex m;

public:
    Counter() {this->n = 0;}
    void inc() {
        this->m.lock();
        this->n++;
        this->m.unlock();
    }
    int getN() {return this->n;}
};

int main() {
    Counter counter;
    std::thread t1([&]{counter.inc();});
    std::thread t2([&]{counter.inc();});
    t1.join();
    t2.join();

    std::cout << "counter n: " << counter.getN() << std::endl;

    return 0;
}
