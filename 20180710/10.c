// Escriba una funcion de C llamada strncat_new que reciba tres parametros:
//  dos punteros a caracter constante (S1 y S2) y un entero sin signo (L).
//
// La funcion debe concatenar S1 con S2 y retornar una nueva cadena de
// caracteres considerando L como tamaño maximo para cualquiera de los
// elementos (S1, S2 y la nueva cadena).
//
// La funcion debe detectar condiciones de erro respecto de la longitud
// y retornar NULL en cualquier caso.
// Detalle

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *strncat_new(const char *S1, const char *S2, unsigned int L) {
    int size_s1 = strlen(S1);
    int size_s2 = strlen(S2);
    int size_res = size_s1 + size_s2;
    if (size_s1 > L || size_s2 > L || size_res > L) {
        return NULL;
    }

    char *res = (char *)malloc(size_res + 1);
    memcpy(res, S1, size_s1);
    memcpy(res + size_s1, S2, size_s2);
    res[size_res] = '\0';
    return res;
}

int main() {
    char S1[] = "hola";
    char S2[] = " mundo";
    char *S3 = strncat_new(S1, S2, 10);

    if (S3 != NULL) {
        printf("S3: %s", S3);
        free(S3);
    }

    return 0;
}
