// Escriba una clase template llamada Sumador que reciba por constructor
// un vector de elementos genericos. La clase Sumador debe incluir un metodo
// llamado sumar que acumule los valores del tipo recibido y retorne un nuevo
// objeto del tipo generico.
//
// Que restricciones se le piden al tipo generico en la definicion de Sumador?
// Detalle

// Restricciones
// - T debe tener un constructor sin parametros que inicialice al objeto en
//   un valor valido para poder acumular objetos en el.
// - T debe tener definido a los operaores + e = (construccion por copia).

#include <iostream>
#include <vector>

template <class T>
class Sumador {
private:
    std::vector<T> elems;

public:
    Sumador(const std::vector<T> &elems): elems(elems) {}
    T sumar() {
        T acum;
        for (const T &e: this->elems) {
            acum = acum + e;
        }
        return acum;
    }
};

int main() {
    std::vector<std::string> v{"hola ", "mundo"};
    Sumador<std::string> s(v);
    std::cout << s.sumar() << std::endl;
}
