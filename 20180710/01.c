// Escriba una rutina que procese un archivo binario indicado por parametro sobre
//  si mismo sumarizando los listados de numeros que posee almacenado. La
//  sumarizacion consiste en recorrer los valores enteros de 32 bits con signo
//  grabados en formato big-endian y acumular sus valores hasta encontrar el
//  valor 0xffffffff que se considera un separador entre listados.
//
// Todos los valores enteros detectados son reemplazados por su sumatoria
// (en el mismo formati) manteniendo luego el elemento separador. Considere
// archivos bien formados.

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>


#define PATHNAME "01-file.dat"

int main() {
    FILE *file;
    int ir = 0;
    int iw = 0;
    int32_t n;
    const int32_t SEPARADOR = -1;
    int32_t sumatoria = 0;
    bool keep_reading = true;

    if ((file = fopen(PATHNAME, "r+")) == NULL) {
        perror("Error fopen: ");
    }

    while (keep_reading) {
        fseek(file, ir, SEEK_SET);
        if (fread(&n, sizeof(n), 1, file) == 1) {
            ir += sizeof(n);
            if (n == SEPARADOR) {
                fseek(file, iw, SEEK_SET);
                fwrite(&sumatoria, sizeof(sumatoria), 1, file);
                fwrite(&SEPARADOR, sizeof(SEPARADOR), 1, file);
                sumatoria = 0;
                iw += 2*sizeof(n);
            } else {
                sumatoria += n;
            }
        } else {
            keep_reading = false;
        }
    }

    fflush(file);
    ftruncate(fileno(file), iw);
    fclose(file);

    return 0;
}
