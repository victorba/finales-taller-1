#ifndef __BUFFERED_INPUT_STREAM_H___
#define __BUFFERED_INPUT_STREAM_H___

#include "socket.h"
#include <stdlib.h>
#include <stdbool.h>

typedef struct __BIS_t {
    socket_t *skt;
    char *buffer;
    int MAX_SIZE;
    int i;
} BIS_t;

// Initializes a buffered input stream on a socket.
// MAX_SIZE is the maximum capacity of buffer.
int BIS_init(BIS_t *bis, socket_t *skt, int MAX_SIZE);

// Reads one character in res.
int BIS_read(BIS_t *bis, char *res);

// Indicates if buffer is empty.
bool BIS_buffer_is_empty(BIS_t *bis);

// Refills buffer with new data.
// Returns 0 on sucess, -1 on error.
int BIS_refill_buffer(BIS_t *bis);


#endif
