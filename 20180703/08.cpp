// La clase Oracion utiliza un char *a para almacenar un string
//  terminado en \0. Escriba la declaracion de esta clase no
//  olvidando: constructor por default, constructor por copia,
//  operadores +, -, =, >> y <<.
//
// Implemente el operador = y el operador -.  Este ultimo debe
// eliminar de la primer cadena todas las ocurrencias de la segunda.
// Detalle

#include <iostream>
#include <string.h>

// Elimina las ocurrencias de elim en s.
void eliminar(char *s, const char *elim) {
    int i = 0;
    int s_size = strlen(s);
    while (s[i] != '\0') {
        if (memcmp(&s[i], elim, strlen(elim)) == 0) {
            memcpy(&s[i], &s[i + strlen(elim)], strlen(&s[i + strlen(elim)]));
            s_size -= strlen(elim);
            s[s_size] = '\0';
        } else {
            i++;
        }
    }
}

class Oracion {
private:
    char *a;

public:
    // Oracion();
    // Oracion(const Oracion &otro);
    // Oracion operator+(const Oracion &otro);
    Oracion(char *a): a(a) {}
    Oracion operator-(const char *otro) {
        eliminar(this->a, otro);
        return *this;
    }

    Oracion &operator=(const Oracion &otro) {
        this->a = otro.a;
        return *this;
    }

    // friend ostream &operator<<(std::ostream &out, const Oracion &o);
    // friend istream &operator>>(std::istream &in, const Oracion &o);
};


int main() {
    char buf[] = "aaabbbbbbbccc";
    eliminar(buf, std::string("b").c_str());
    std::cout << buf << std::endl;
}
