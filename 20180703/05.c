// Dentro del siguiente codigo:

// Defina:
//
// a) Un puntero a entero sin signo a alocarse en el stack
//
// b) Un arreglo para albergar 4 numeros de punto flotante que se aloque en el data segment.
//
// c) Un caracter a alocarse en el data segment.

// Array de 4 numeros de punto flotante en el data segment
float array[4];

// Un caracter alocado en el data segment.
char caracter;

int main(int argc, char* argv[]) {
    // Puntero a entero sin signo alocado en el stack
    unsigned int *ptr = 0;
    ptr++;
    return 0;
}
