// Implemente la funcion void String_a_Int(char *bin, int *ent)
// que interprete la cadena bin (de 32 1s/0s) y guarde el valor
// correspondiente en el entero indicado por ent.
// Detalle

#include <assert.h>
#include <stdio.h>

void String_a_Int(char *bin, int *ent) {
    int res = 0;
    for (int i = 0; i < 32; i++) {
        res = res << 1;
        if (bin[i] == '1') {
            res = res | 1;
        }
    }
    *ent = res;
}

int main() {
    // 1
    char bin[] = "00000000000000000000000000000001";
    int ent;
    String_a_Int(bin, &ent);
    printf("ent: %d\n", ent);

    // -1
    char bin1[] = "11111111111111111111111111111111";
    int ent1;
    String_a_Int(bin1, &ent1);
    printf("ent1: %d\n", ent1);

    return 0;
}
