#include "socket.h"

int socket_client_init(
    socket_t *skt,
    const char node[],
    const char service[]) {
    struct addrinfo hints;
    struct addrinfo *res;
    struct addrinfo *ptr;
    int r;
    int s;
    bool are_we_connected = false;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = 0;

    if ((r = getaddrinfo(node, service, &hints, &res) < 0)) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(r));
        return ERROR_CODE;
    }

    for (ptr = res; ptr != NULL && !are_we_connected; ptr = ptr->ai_next) {
        s = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
        if (s != -1) {
            if ((r = connect(s, ptr->ai_addr, ptr->ai_addrlen)) != -1) {
                are_we_connected = true;
            }
        }
    }

    freeaddrinfo(res);

    if (!are_we_connected) {
        fprintf(stderr, "socket: impossible to connect\n");
        return ERROR_CODE;
    }

    skt->sockfd = s;
    return SUCCESS_CODE;
}

int socket_server_init(
    socket_t *skt,
    const char service[]) {
    struct addrinfo hints;
    struct addrinfo *res;
    struct addrinfo *ptr;
    int r;
    int s;
    bool are_we_bound = false;
    int val = 1;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    if ((r = getaddrinfo(NULL, service, &hints, &res)) < 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(r));
        return ERROR_CODE;
    }

    for (ptr = res; ptr != NULL && !are_we_bound; ptr = ptr->ai_next) {
        s = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
        if (s != -1) {
            r = setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
            if (r != -1) {
                if ((r = bind(s, ptr->ai_addr, ptr->ai_addrlen)) != -1) {
                    are_we_bound = true;
                }
            }
        }
    }

    freeaddrinfo(res);

    if (!are_we_bound) {
        fprintf(stderr, "socket: impossible to connect\n");
        return ERROR_CODE;
    }

    skt->sockfd = s;
    return SUCCESS_CODE;
}

int socket_listen(socket_t *skt, int backlog) {
    int r;
    if ((r = listen(skt->sockfd, backlog)) == -1) {
        fprintf(stderr, "listen: %s\n", strerror(errno));
        return ERROR_CODE;
    }
    return SUCCESS_CODE;
}

int socket_accept(socket_t *skt, socket_t *accepted_skt) {
    int s;
    if ((s = accept(skt->sockfd, NULL, NULL)) == -1) {
        fprintf(stderr, "accept: %s\n", strerror(errno));
        return ERROR_CODE;
    }

    accepted_skt->sockfd = s;
    return SUCCESS_CODE;
}

int socket_close(socket_t *skt) {
    int r;
    if ((r = close(skt->sockfd)) == -1) {
        fprintf(stderr, "close: %s\n", strerror(errno));
        return ERROR_CODE;
    }
    return SUCCESS_CODE;
}

int socket_send_all(socket_t *skt, const char buf[], unsigned int size) {
    unsigned int sent = 0;
    int r = 0;
    bool there_was_an_error = false;

    while (sent < size && !there_was_an_error) {
        r = send(skt->sockfd, &buf[sent], size - sent, MSG_NOSIGNAL);
        if (r == -1) {
            fprintf(stderr, "send: %s\n", strerror(errno));
            there_was_an_error = true;
        } else {
            sent += r;
        }
    }

    if (there_was_an_error) {
        return ERROR_CODE;
    } else {
        return sent;
    }
}

int socket_recv_all(socket_t *skt, char buf[], unsigned int size) {
    unsigned int received = 0;
    int r = 0;
    bool there_was_an_error = false;
    bool is_the_socket_valid = true;

    while (received < size && !there_was_an_error && is_the_socket_valid) {
        r = recv(skt->sockfd, &buf[received], size - received, 0);
        if (r == -1) {
            fprintf(stderr, "recv: %s\n", strerror(errno));
            there_was_an_error = true;
        } else if (r == 0) {
            is_the_socket_valid = false;
        } else {
            received += r;
        }
    }

    if (there_was_an_error) {
        return ERROR_CODE;
    } else {
        return received;
    }
}

int socket_shutdown(socket_t *skt, int how) {
    int r;
    if ((r = shutdown(skt->sockfd, how)) == -1) {
        fprintf(stderr, "shutdown: %s\n", strerror(errno));
        return ERROR_CODE;
    }
    return SUCCESS_CODE;
}

int socket_send(socket_t *skt, const void *buf, size_t len, int flags) {
    int r;
    if ((r = send(skt->sockfd, buf, len, flags)) == -1) {
        fprintf(stderr, "send: %s\n", strerror(errno));
    }
    return r;
}

int socket_recv(socket_t *skt, void *buf, size_t len, int flags) {
    int r;
    if ((r = recv(skt->sockfd, buf, len, flags)) == -1) {
        fprintf(stderr, "recv: %s\n", strerror(errno));
    }
    return r;
}
