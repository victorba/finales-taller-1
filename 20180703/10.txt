// Que es un deadlock? Ejemplifique.

// Un deadlock es un estado en el cual cada miembro de un grupo
// tiene un recurso y esta esperando a que otro libere tambien un recurso
// o lock.

// ejemplo: Dos secuencias de instrucciones

    T1              T2
    M1.LOCK()
                    M2.LOCK()
                    M1.LOCK()
    M2.LOCK()

Ambos hilos estan bloqueados y esperando a que el otro libere al lock.
