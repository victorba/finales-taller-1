// Escriba un programa que reciba paquetes de 10 bytes por el puerto
// TCP 815 y los imprima por pantalla. Al recibir el byte 0xff debe
// cerrarse ordenadamente. No considere errore.

#include "socket.h"
#include <stdbool.h>
#include <string.h>

#define NODE "localhost"
#define SERVICE "4546"
#define MAX_BUFFER 10

typedef struct __socket0_t {
    int sockfd;
} socket0_t;

int socket0_server_init(socket0_t *skt, char service[]);
int socket0_listen(socket0_t *skt, int backlog);
int socket0_accept(socket0_t *skt, socket0_t *peer);
int socket0_recv_all(socket0_t *skt, char *buf, int size);
int socket0_close(socket0_t *skt);
void test();

int main(int argc, char *argv[]) {
    if (argc > 1) {
        test();
        return 0;
    }
    socket0_t sv;
    socket0_t peer;
    char buf[MAX_BUFFER + 1];
    buf[MAX_BUFFER] = '\0';
    bool keep_receiving = true;
    char end[] = {0xff};

    socket0_server_init(&sv, SERVICE);
    socket0_listen(&sv, 100);
    socket0_accept(&sv, &peer);

    while (keep_receiving) {
        socket_recv_all((socket_t *)&peer, buf, MAX_BUFFER);

        for (int i = 0; i < MAX_BUFFER; i++) {
            if (memcmp(&buf[i], end, 1) == 0) {
                keep_receiving = false;
                buf[i] = '\0';
            }
        }

        printf("buffer: %s\n", buf);
    }

    socket0_close(&peer);
    socket0_close(&sv);

    return 0;
}

int socket0_server_init(socket0_t *skt, char service[]) {
    struct addrinfo hints;
    struct addrinfo *res;
    struct addrinfo *ptr;
    bool are_we_bound = false;
    int r;
    int sockfd;
    int val = 1;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    if ((r = getaddrinfo(NULL, service, &hints, &res)) == -1) {
        fprintf(stderr, "Error getaddrinfo: %s\n", gai_strerror(r));
        return -1;
    }

    for (ptr = res; ptr != NULL && !are_we_bound; ptr = ptr->ai_next) {
        sockfd = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
        if (sockfd != -1) {
            setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
            r = bind(sockfd, ptr->ai_addr, ptr->ai_addrlen);
            if (r != -1) {
                are_we_bound = true;
            }
        }
    }

    freeaddrinfo(res);

    if (!are_we_bound) {
        fprintf(stderr, "Error no se pudo conectar:");
        return -1;
    }

    skt->sockfd = sockfd;
    return 0;
}

int socket0_listen(socket0_t *skt, int backlog) {
    return listen(skt->sockfd, backlog);
}
int socket0_accept(socket0_t *skt, socket0_t *peer) {
    int sockfd;
    sockfd = accept(skt->sockfd, NULL, NULL);
    peer->sockfd =sockfd;
    return sockfd;
}
int socket0_recv_all(socket0_t *skt, char *buf, int size) {
    int received = 0;
    bool there_was_an_error = false;
    bool is_the_socket_valid = true;
    int r;

    while (received < size && !there_was_an_error && is_the_socket_valid) {
        r = recv(skt->sockfd, &buf[received], size - received, 0);
        if (r == -1) {
            there_was_an_error = true;
        } else if (r == 0) {
            is_the_socket_valid = false;
        } else {
            received += r;
        }
    }

    if (there_was_an_error) {
        return -1;
    } else {
        return received;
    }

    return 0;
}

int socket0_close(socket0_t *skt) {
    return close(skt->sockfd);
}

void test() {
    socket_t cli;
    char buf[] = {  'a', 'a', 'a', 'a', 'a',
                    'b', 'b', 'b', 'b', 'b',
                    'c', 'c', 'c', 'c', 'c',
                    'e', 'e', 'e', 'e', 0xff};

    socket_client_init(&cli, NODE, SERVICE);
    socket_send_all(&cli, buf, sizeof(buf));
    socket_close(&cli);
}
