// Que es la compilacion condicional? En que etapa del proceso de
// transformacion de codigo se resuelve? Ejemplifique mediante un codigo C
// dando un caso de uso util.
// Detalle


// La compilacion condicional es tipo de compilacion que compila cierto
// codigo bajo alguna condicion.
// Se resuelve en la precompilacion en la etapa de sustitucion y control
// de la precompilacion.

// Ejemplo:
// X inicialmente se define como 10 entonces el archivo resultado de la
// precompilacion va a tener
// - printf("X esta definido") porque se definio X
// - printf("Y no esta definido") porque no esta defindo Y en el archivo.
// - printf("X ya no esta definido") dado que con #undef se le quita la
//   definicion.

#include <stdio.h>

#define X 10

int main() {
    #ifdef X
    printf("X esta definido\n");
    #else
    printf("X no esta definido\n");
    #endif

    #ifndef Y
    printf("Y no esta definido\n");
    #endif

    #undef X
    #ifdef X
    printf("X sigue estando definido\n");
    #else
    printf("X ya no esta definido\n");
    #endif

    return 0;
}
