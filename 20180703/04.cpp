// Indicar la salida del siguiente programa:

// Derivada.f1
// Derivada.f2
// Derivada.f1
// Base.f1
// Derivada.f2
// Derivada.f1

// Nota: Los metodos estaticos no necesitan de un objeto y por lo tanto no
//       funciona muy bien lo de polimorfismo que si depende de que objeto
//       se este llamando.
//       Entonces, nos metodos estaticos NO PUEDEN SER VIRTUAL! asi entonces
//       si se llama a un metodo estatico de una clase, simplemente se llama
//       sin ver lo de polimorfismo.

#include <iostream>

using namespace std;

class Base {

public:

    static void f1() { cout << "Base.f1" << endl; }

    virtual void f2() { cout << "Base.f2" << endl; f1(); }

    virtual void f3() { cout << "Base.f3" << endl; f2(); f1(); }

};

class Derivada : public Base {

public:

    static void f1() { cout << "Derivada.f1" << endl; }

            void f2() { cout << "Derivada.f2" << endl; f1(); }

            void f3() { cout << "Derivada.f3" << endl; f2(); f1(); }

};

int main() {

   Derivada D;

   Base*  pB = &D;

   Derivada *pD = &D;

    pD->f1();

    pD->f2();

    pB->f1();

    pB->f2();

}
