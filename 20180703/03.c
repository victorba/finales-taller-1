// Escribir un programa que procese un archivo binario de enteros
// sin signo sobre si mismo. El procesamiento consiste en leer pares
// de enteros de 1 byte cada uno y reemplazarlos por 3 enteros (el archivo
// se agranda): su suma, su resta y el OR logico entre ambos.
// Detalle

#define PATHNAME "03.bin"

#include <stdio.h>
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>

typedef struct __pair_t {
    int8_t x;
    int8_t y;
} __attribute__((packed)) pair_t;

typedef struct __res_t {
    int8_t suma;
    int8_t resta;
    int8_t or;
} __attribute__((packed)) res_t;

int main() {
    FILE *file;
    int iread;
    int iwrite;
    int old_size;
    int cant_pair;
    int new_size;
    pair_t pair;
    res_t res;

    if ((file = fopen(PATHNAME, "r+")) == NULL) {
        perror("Error fopen: ");
        return -1;
    }

    iread = fseek(file, 0, SEEK_END);
    old_size = ftell(file);
    iread = old_size;
    iread -= sizeof(pair);

    cant_pair = old_size/sizeof(pair);
    new_size = cant_pair * sizeof(res);

    ftruncate(fileno(file), new_size);

    fseek(file, 0, SEEK_END);
    iwrite = ftell(file);
    iwrite -= sizeof(res);

    while (iwrite >= 0) {
        fseek(file, iread, SEEK_SET);
        fread(&pair, sizeof(pair), 1, file);
        iread -= sizeof(pair);
        res.suma = pair.x + pair.y;
        res.resta = pair.x - pair.y;
        res.or = pair.x | pair.y;
        fseek(file, iwrite, SEEK_SET);
        fwrite(&res, sizeof(res), 1, file);
        iwrite -= sizeof(res);
    }

    fclose(file);

    return 0;
}
