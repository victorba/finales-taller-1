Cual es el uso de la funcion listen? Que parametros tiene y para que sirven?

Sirve para marcar al socket como pasivo, es decir uno que será usado para recibir una conexion usando accept().

Los parametros son
- sockfd: El file descriptor del socket.
- backlog: Define el tamaño maximo de la cola de conexiones pendientes para el
           socket. Si una peticion de conexion llega cuando la cola
           esta llena, el cliente podria recibir un error.
