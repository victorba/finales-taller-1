#include "mapper.h"
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

#define REG_SIZE 4

long file_size(FILE *file) {
  long prev;
  long size;

  prev = ftell(file);
  fseek(file, 0, SEEK_END);
  size = ftell(file);
  fseek(file, prev, SEEK_SET);

  return size;
}

void file_cpy(FILE *file, long dst_offset, long src_offset, long size) {
  long prev;
  char buffer;

  prev = ftell(file);
  for (long i = 0; i < size; i++) {
    fseek(file, src_offset, SEEK_SET);
    fread(&buffer, sizeof(buffer), 1, file);
    fseek(file, dst_offset, SEEK_SET);
    fwrite(&buffer, sizeof(buffer), 1, file);
    src_offset += sizeof(buffer);
    dst_offset += sizeof(buffer);
  }

  fseek(file, prev, SEEK_SET);
}

int char_hex_to_dec(char c) {
  int n;

  if (c >= 'A' && c <= 'F') {
    n = c - 'A' + 10;
  } else {
    n = c - '0';
  }

  return n;
}

void hex_to_dec(char input[], char output[]) {
  long n;
  long size = strlen(input);
  long total = 0;
  int base = 16;

  for (int i = 0; i < size; i++) {
    n = char_hex_to_dec(input[size - i - 1]);
    total += n * pow(base, i);
  }

  sprintf(output, "%ld", total);
}

void file_map(char path[], void (*f)(char input[], char output[])) {
  FILE *file;
  long fsize;
  long registers;
  long offset;
  char buffer[REG_SIZE + 1];
  int maxoutput = 100;
  char output[maxoutput];

  file = fopen(path, "r+");
  fsize = file_size(file);
  registers = fsize / REG_SIZE;
  memset(buffer, 0, REG_SIZE + 1);

  offset = 0;

  for (int i = 0; i < registers; i++) {
    fseek(file, offset, SEEK_SET);
    fread(buffer, REG_SIZE, 1, file);
    buffer[REG_SIZE] = '\0';
    memset(output, '\0', maxoutput);
    f(buffer, output);
    file_cpy(
      file,
      offset + strlen(output),
      offset + REG_SIZE,
      fsize - (offset + REG_SIZE)
    );
    fseek(file, offset, SEEK_SET);
    fwrite(output, strlen(output), 1, file);
    offset += strlen(output);
  }

  fflush(file);
  if (file_size(file) > fsize) {
    // No hay forma de truncar en ISO C.
    truncate(path, offset);
  }

  fclose(file);
}
