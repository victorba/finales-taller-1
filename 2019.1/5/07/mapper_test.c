#include <criterion/criterion.h>
#include "mapper.h"
#include <stdio.h>
#include <stdbool.h>

Test(test, should_return_file_size) {
  FILE *file;
  file = fopen("./input.txt", "r");

  long size = file_size(file);
  fclose(file);

  cr_expect_eq(20, size);
}

Test(test, should_copy_file) {
  system("cp input01.txt input01copytest.txt");
  FILE *file;
  file = fopen("input01copytest.txt", "r+");

  file_cpy(file, 1, 4, 4);
  fclose(file);

  cr_expect_eq(
    0,
    system("diff input01copytest.txt output01.txt")
  );
  system("rm input01copytest.txt");
}

Test(test, should_convert_char_hex_to_dec) {
  cr_expect_eq(10, char_hex_to_dec('A'));
}

Test(test, should_convert_hex_to_dec) {
  char input[] = "0001";
  int size = 10;
  char output[size];
  memset(output, 0, size);

  hex_to_dec(input, output);

  cr_expect_eq(
    0,
    strncmp("1", output, 1)
  );
}

Test(test, should_convert_hex_to_dec_big_numbers) {
  char input[] = "FFFF";
  int size = 10;
  char output[size];
  memset(output, 0, size);

  hex_to_dec(input, output);

  char expected[] = "65535";
  cr_expect_eq(
    0,
    strncmp(expected, output, strlen(expected))
  );
}

Test(test, should_map_hex_to_dec) {
  system("cp input.txt inputmap.txt");

  file_map("./inputmap.txt", hex_to_dec);

  cr_expect_eq(
    0,
    system("diff inputmap.txt output.txt")
  );
}
