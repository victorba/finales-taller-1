#ifndef __MAPPER_H__
#define __MAPPER_H__

#include <stdio.h>

long file_size(FILE *file);
void file_cpy(FILE *file, long dst_offset, long src_offset, long size);
void file_map(char path[], void (*f)(char input[], char output[]));
int char_hex_to_dec(char c);
void hex_to_dec(char input[], char output[]);

#endif
