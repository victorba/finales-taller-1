#include <string>
#include <iostream>
#include <cmath>


// OBS: Era solo declarar.
//      Solo definir operaor>>
class Telefono {
private:
  long area;
  long numero;

public:
  Telefono(const long &area, const long &numero):
    area(area),
    numero(numero) {}

  Telefono(Telefono &&other) {
    area = std::move(other.area);
    numero = std::move(other.numero);
  }

  Telefono(const Telefono &other) {
    area = other.area;
    numero = other.numero;
  }

  friend std::ostream &operator<<(std::ostream &out, const Telefono &telefono) {
    out << telefono.area << " " << telefono.numero;
    return out;
  }

  friend std::istream &operator>>(std::istream &in, Telefono &telefono) {
    in >> telefono.area;
    in >> telefono.numero;
    return in;
  }

  bool operator==(const Telefono &other) {
    return area == other.area && numero == other.numero;
  }

  Telefono &operator=(const Telefono &other) {
    area = other.area;
    numero = other.numero;
    return *this;
  }

  operator long() {
    return (long)(area * std::pow(10, std::to_string(numero).size()) + numero);
  }
};

int main() {
  return 0;
}
