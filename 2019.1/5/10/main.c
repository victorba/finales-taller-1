#include "duplicate.h"
#include <string.h>
#include <stdio.h>

#define MAXBUFFER 100

int main(int argc, char *argv[]) {
  char buffer[MAXBUFFER];
  memset(buffer, '\0', MAXBUFFER);

  duplicate(argv[1], argv[2], buffer);
  printf("%s", buffer);
}
