#include <criterion/criterion.h>
#include <string.h>
#include "duplicate.h"
#include <stdio.h>

#define MAXBUFFER 100

Test(test, should_duplicate) {
  char buffer[MAXBUFFER];
  memset(buffer, '\0', MAXBUFFER);

  duplicate(
    "el final esta aprobado",
    "aprobado",
    buffer
  );

  char expected[] = "el final esta aprobado aprobado";
  cr_expect_eq(0, strncmp(expected, buffer, strlen(expected)));
}

Test(test, should_lengths_be_equal) {
  char buffer[MAXBUFFER];
  memset(buffer, '\0', MAXBUFFER);

  duplicate(
    "el final esta aprobado",
    "aprobado",
    buffer
  );

  char expected[] = "el final esta aprobado aprobado";
  cr_expect_eq(strlen(expected), strlen(buffer));
}

Test(test, should_receive_string_by_command_line) {
  system("./app.app 'el final esta aprobado' 'aprobado' > output01.txt");
  cr_expect_eq(
    0,
    system("diff output.txt output01.txt")
  );
}

Test(test, should_duplicate_every_word) {
  char buffer[MAXBUFFER];
  memset(buffer, '\0', MAXBUFFER);

  duplicate(
    "el final esta aprobado",
    "aprobado final",
    buffer
  );

  char expected[] = "el final final esta aprobado aprobado";
  cr_expect_eq(0, strncmp(expected, buffer, strlen(expected)));
}
