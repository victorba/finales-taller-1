#include "duplicate.h"
#include <string.h>
#include <stdio.h>

void duplicate(char src[], char substr[], char dst[]) {
  char *ptr;
  strncpy(dst, src, strlen(src));

  while ((ptr = strstr(dst, substr)) != NULL) {
    memcpy(
      &ptr[strlen(substr) + 1],
      ptr,
      strlen(ptr)
    );
    ptr[strlen(substr)] = ' ';
    dst = ptr + 2*strlen(substr) + 1;
  }
}
