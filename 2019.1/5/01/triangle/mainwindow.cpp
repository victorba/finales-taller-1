#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPainter>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *e) {
    QPainter painter(this);
    QPolygon poly;

    painter.setBrush(Qt::yellow);
    poly << QPoint(0, 0) << QPoint(this->width(), 0)
         << QPoint(this->width() / 2, this->height());
    painter.drawPolygon(poly);
}

