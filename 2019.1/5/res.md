# 13 / 08 / 2019

1. Escriba una rutina (para ambiente gráfico Windows o Linux) que dibuje un triángulo amarillo del tamaño de la ventana.
  *ver 01*

2. Escriba las siguientes definiciones/declaraciones:    
  A) Declaración de un puntero a puntero a entero largo con signo.    
  B) Definición de una la función RESTA, que tome dos enteros largos con signo y devuelva su resta. Esta función debe ser visible en todos los módulos del programa.    
  C) Definición de un caracter solamente visible en el módulo donde se define.     

  *Respuestas*    
  ```
  // a) Puntero a entero largo con signo
  extern long int *x;

  // b) En archivo resta.h. Debe ser incluido en cada archivo que
  //    invoque la funcion.
  long int resta(long int x, long int y);
  // resta.c -> Definicion.
  long int resta(long int x, long int y) {
    return x - y;
  }

  // c) Fuera de cualquier funcion.
  static char c;
  ```

3. Declare la clase TELEFONO para encapsular una cadena numérica correspondiente a un teléfono. Incluya al menos: Constructor(area, numero), Constructor move y Constructor de Copia; Operador <<, ==, =, long y >>. Implemente el operador >>.     
  *ver 03*

4. Explique qué se entiende por “compilación condicional”. Ejemplifique mediante código.
  Se entiende por compilacion condicional al proceso que permite generar distintos ejecutables mediante parametros durante la compilacion. Hay seis directivas usadas:
  ```
  #define
  #ifdef
  #ifndef
  #else
  #elif
  #endif
  ```
  Ejemplo:
  ```
  #define X 10

  int main() {
      #ifdef X
      printf("X is: %d\n", X);
      #else
      printf("X does not have a value");
      #endif
      return 0;
  }
  ```

5. ¿Qué significa que una función es blocante?¿Cómo subsanaría esa limitación en término de mantener el programa ‘vivo’ ?
  Una función es bloqueante cuando detiene la ejecucion de de un hilo/proceso hasta un determinado evento (ej: recibir un byte mediante recv()). Para subsanar la limitacion se puede invocar a la funcion en un hilo aparte de tal manera que no detenga al hilo principal.

6. Explique qué son los métodos virtuales puros y para qué sirven. De un breve ejemplo donde su uso sea imprescindible.    
  Los metodos virtuales puros son metodos definidos en clases bases y deben ser redefinidos en las clases deririvadas. En C++ se antepone la palabra reservada ```virtual``` y al final ```=0```. Sirven para utilizar Runtime polymorphism y reforza la definicion de metodos en las clases derivadas.
  Es imprescindible cuando se utiliza polimorfimo, no se encuentra una definicion en la clase base y cada clase derivada presenta un comportamiento distinto ante un metodo.

7. Escribir un programa C que procese el archivo “numeros.txt” sobre sí mismo (sin crear archivos intermedios y sin subir el archivo a memoria). El procesamiento consiste en leer nros hexadecimales de 4 símbolos y reemplazarlos por su valor decimal (en texto).   
  *ver 07*

8. ¿Qué es el polimorfismo? Ejemplifique mediante código.    
  Es el concepto en el que distintos objetos pueden ser accedidos mediante una misma interfaz. Cada tipo puede proveer su propia implementación. Ejemplo:
  ```
  class Animal {
    virtual public std::string sound() = 0;
  };

  class Cat: public Animal {
    virtual public std::string sound(){ return "Mew"; }
  }

  class Dog: public Animal {
    virtual public std::string sound(){ return "Woof"; }
  }

  int main() {
    std::list<Animal> animals({ Dog(), Cat() });
    for (Animal a: animals) {
      std::cout << a.sound() << std::endl;
    }
    return 0;
  }
  ```

9. ¿Qué función utiliza para esperar la terminación de un thread? Ejemplifique mediante código
  Se utiliza a join.
  ```
  void print() {
    std::cout << "Printing..." << std::endl;
  }

  int main() {
    std::thread t(print, NULL);
    t.join();
    return 0;
  }
  ```

10. Escriba un programa C que tome 2 cadenas por línea de comandos: A y B; e imprima la cadena A después de haber duplicado todas las ocurrencias de B.. ej.: reemp.exe “El final está aprobado” aprobado -----> El final está aprobado aprobado
