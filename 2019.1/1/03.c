#include <stdio.h>

// Puntero a funcion que recibe un entero por parametro y devuelve
// nada.
void (*F)(int i);

void print(int i) {
  printf("This int is %d\n", i);
}

// Funcion B estatica. Solo puede ser accedida desde este archivo.
// Recibe dos floats y devuelve nada.
static void B(float a, float b) {}

// C es un puntero a un array de 5 punteros a int.
// Se lee *C -> C es un puntero.
// Reemplazo C por X -> int *X[5] --> X es un array de 5 punteros a int.
// ===> C es un puntero a un array de 5 punteros a int.
int *(*C)[5];

int main() {
  int i = 100;

  F = print;
  F(i);

  B(1.2, 3.4);

  int *X[5];
  C = &X;
  printf("C is pointing to %p\n", C);
  printf("Size of C: %ld\n", sizeof(C));
  return 0;
}
