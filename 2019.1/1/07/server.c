#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define BACKLOG 1
#define MAXBUFFER 10

int main(int argc, char *argv[]) {
  const char *port = argv[1];
  const char *ip = argv[2];

  struct addrinfo hints;
  struct addrinfo *res;
  struct addrinfo *ptr;
  int sockfd;
  int peerfd;
  bool are_we_binded = false;
  int val = 1;

  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE;

  getaddrinfo(ip, port, &hints, &res);

  for (ptr = res; ptr != NULL && !are_we_binded; ptr = ptr->ai_next) {
    sockfd = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
    if (sockfd != -1) {
      setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
      if (bind(sockfd, ptr->ai_addr, ptr->ai_addrlen) != -1) {
        are_we_binded = true;
      }
    }
  }

  freeaddrinfo(res);

  if (!are_we_binded) {
    return -1;
  }

  listen(sockfd, BACKLOG);
  peerfd = accept(sockfd, 0, 0);

  // recv datos

  bool keep_going = true;
  bool keep_reading;
  char c;
  char buffer[MAXBUFFER];
  int i = 0;
  int total;

  while (keep_going) {
    keep_reading = true;
    total = 0;
    while (keep_reading) {
      recv(peerfd, &c, sizeof(c), 0);
      printf("c: %c\n", c);
      if (c == ',') {
        buffer[i] = '\0';
        total += atoi(buffer);
        i = 0;
      } else if (c == '=') {
        buffer[i] = '\0';
        total += atoi(buffer);
        i = 0;
        keep_reading = false;
        printf("Total: %d\n", total);
      } else {
        buffer[i] = c;
        i++;
        if (memcmp(buffer, "FIN", 3) == 0) {
          keep_reading = false;
          keep_going = false;
        }
      }
    }
  }

  // Fin
  close(peerfd);
  close(sockfd);

  return 0;
}
