#include "socket.h"

#define NODE "localhost"
#define PORT "4444"

int main() {
  socket_t socket;
  socket_client_init(&socket, NODE, PORT);

  char text[] = "27,12,32=FINXXXXXX";
  socket_send_all(&socket, text, sizeof(text));

  socket_shutdown(&socket, SOCKET_SHUT_RDWR);
  socket_close(&socket);
  return 0;
}
