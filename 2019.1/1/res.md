# 01/07/2019

1. Explique qué es y para qué sirve un constructor de copia en C++.     
  a) Indique cómo se comporta el sistema si éste no es definido por el desarrollador;   
  b) Explique al menos una estrategia para evitar que una clase particular sea copiable;   
  c) Indique qué diferencia existe entre un constructor de copia y uno move.   

  Un constructor por copia es un constructor que se utiliza para inicializar a un objeto en base a uno previamente construido. Es util cuando:
  - Se quiere copiar un objeto ya construido
  - Se quiere devolver una copia de un objeto de una funcion
  - Se quiere pasar una copia a una funcion

  a) Si no es definido, al construir por copia se copia cada uno de los valores del original al nuevo objeto.    
  b) Se puede marcar como como ```delete```. Tambien se puede dejar privado.    
  c) Un constructor por movimiento se "adueña" de los elementos del objeto origen dejandolo vacio y mientras que en el por copia ambos quedan con los mismos valores. La sintaxis del constructor por moviemiento usa && mientras que el de copia usa & (referencias). Los constructores por movimiento son mas eficientes dado que evita tener que copiar dos veces los mismos elementos.

2. Escriba una función ISO C llamada Replicar que reciba 1 cadena (S), dos índices (I1 e I2) y una cantidad (Q). La función debe retornar una copia de S salvo los caracteres que se encuentran entre los índices I1 e I2 que serán duplicados Q veces. Ej. replicar(“Hola”, 1, 2, 3) retorna “Hololola”.   

3. Describa con exactitud las siguientes declaraciones/definiciones globales:
  ```
  1. void (*F)(int i);
  2. static void B(float a, float b){}
  3. int *(*C)[5];
  ```
  *ver 03.c*

4. Escribir un programa ISO C que reciba por argumento el nombre de un archivo de texto y lo procese sobre sí mismo (sin crear archivos intermedios ni subiendo todo su contenido a memoria). El procesamiento consiste en eliminar las líneas de 1 sola palabra.
  *ver 04*

5. Declare una clase de elección libre. Incluya todos los campos de datos requeridos con su correcta exposición/publicación, y los operadores ++, -, ==, >> (carga), << (impresión), constructor move y operador float().
  *ver 05.cpp*

6. ¿Qué es una macro de C? Describa las buenas prácticas para su definición y ejemplifique.    
 Las macros en c son porciones de codigo con nombre.    
 Se considera como buenas practicas:
  - Deben tener nombres representativos y en mayuscula
  ejemplo:
  ```
  #define xafs 5 // mal
  #define MAXBUFFER 255 // bien
  ```
  - Si son usadas para definir function-like-macros se aconseja utilizar parentesis () en los argumentos dado que los argumentos no son evaluados antes de ejecutar la macro. No deben terminar en ";" para evitar cortar comandos y generar problemas de sintaxis.
  ```
  #define MULT(X, Y) X * Y
  // Mal porque MULT(2+2, 3+3) en vez de ser 4*6 = 24 se evalua cmo
  // 2+2*3+3 = 2 + 6 + 3 = 11.
  #define MULT(X, Y) (X) * (Y)
  // Bien porque ahora MULT(2+2, 3+3) es (2+2)*(3+3) = 4*6 = 24.
  ```
  ```
  #define INC(X) (X + 1);
  int x = 2 + INC(3) + 4;
  // Mal porque se ejecuta como int x = 2 + (3 + 1); 4;
  // dando problemas de sintaxis.
  #define INC(X) (X + 1)
  int x = 2 + INC(3) + 4
  // Bien porque se ejecuta como int x = 2 + (3 + 1) + 4;
  ```

7. Escriba un programa que reciba por línea de comandos un Puerto y una IP. El programa debe aceptar una única conexión e imprimir en stdout la sumatoria de los enteros recibidos en cada paquete. Un paquete está definido como una sucesión de números recibidos como texto, en decimal, separados por comas y terminado con un signo igual (ej: “27,12,32=”). Al recibir el texto ‘FIN’ debe finalizar el programa ordenadamente liberando los recursos.
  *ver 07*

8. Describa el proceso de transformación de código fuente a un ejecutable. Precise las etapas, las tareas desarrolladas y los tipos de error generados en cada una de ellas.
  1. Precompilacion: Se realizan las siguientes subetapas.   
    1.1 Sustitucion y control: Se incluyen (o dejan de) archivos y bloques de codigo. Es del tipo find and replace y la macro mas utilizada es ```#define```. Errores comunes: Inclusiones ciclicas, no incluir un archivo necesario, definir mal constantes.   
    1.2 Expansion de macros: Reemplaza el simbolo de las macros por su codigo. Errores comunes: Mal definidas, pasa de parametros erroneo.    
  2. Compilacion: Su fin es convertir los archivos a codigo objeto. Cuenta de tres etapas:   
    2.1 Parsing: Consiste en realizar un arbol semantico. Errores comunes: No cerrar bloques de codigo.
    2.2 Translation: Traducir los simbolos a lenguaje ensamblador. Errores comunes: Escribir insturcciones invalidas.
    2.3 Ensamblado: Obtener codigo objeto. Errores: Instrucciones no soportadas por el procesador.
  3. Linking: Consiste en resolver simbolos no resueltos en la etapa de compilacion. Hay de dos tipos:
    - Static: Los simbolos se resuelven en tiempo de compilacion y se almacenan en el mismo ejecutable.
    - Dinamico: Se resuelven en run-time y se invocan las bibliotecas necesarias.
    Errores comunes: Invocar codigo no definido, utilizar codigo de bibliotecas sin haberlas incluido.

9. ¿Qué ventaja ofrece un lock raii frente al tradicional lock/unlock?
  Presenta la ventaja de liberar el recurso de forma automatica tras terminar su vida, en cambio, un lock tradicional es suceptible a no liberar el lock al terminar un bloque de codigo o al salir de él.

10. ¿Qué significa que una función es blocante?¿Cómo subsanaría esa limitación en términos de mantener el programa ‘vivo’?
   Una funcion es bloqueante cuando detiene la ejecución de un proceso/hilo hasta un evento, como puede ser la liberación de un recurso.
   Para subsanar, se puede realizar la invocacion en un hilo aparte de tal forma que no bloquee al hilo principal.
