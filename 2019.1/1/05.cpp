// Declare una clase de elección libre. Incluya todos los campos
// de datos requeridos con su correcta exposición/publicación, y
//  los operadores ++, -, ==, >> (carga), << (impresión), constructor
//   move y operador float()

#include <iostream>

class Number {
private:
  int n;

public:
  Number(int n): n(n) {}
  Number(Number &&other) { this->n = other.n; other.n = 0; }
  int getValue() { return n; }
  void operator++() { ++ n; }
  Number operator++(int) { n++; return n; }
  bool operator==(const Number &other) { return this->n == other.n; }
  friend std::ostream &operator<<(std::ostream &out, const Number &n);
  friend std::istream &operator>>(std::istream &in, Number &n);
  operator float() { return (float)n; }
};

std::ostream &operator<<(std::ostream &out, const Number &n) {
  out << "the number n is: " << n.n << "  ";
  return out;
}

std::istream &operator>>(std::istream &in, Number &n) {
  in >> n.n;
  return in;
}

int main() {
  Number n(100);
  Number other(555);
  std::cout << "N = " << n.getValue() << std::endl;

  n++;
  std::cout << "N = " << n.getValue() << std::endl;

  ++n;
  std::cout << "N = " << n.getValue() << std::endl;

  std::cout << "n == other? " << (n == other) << std::endl;

  std::cout << n << std::endl;

  std::cout << "Enter new n value" << std::endl;
  std::cin >> n;
  std::cout << "n = " << n.getValue() << std::endl;

  Number x(std::move(n));
  std::cout << "x: " << x << "  and n: " << n << std::endl;

  std::cout << "other as float " << ((float) other) << std::endl;

  return 0;

}
