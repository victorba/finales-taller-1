#include <criterion/criterion.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

char *replicar(char *S, int I1, int I2, int Q) {
  int extralen = (I2 - I1 + 1) * (Q - 1);
  int new_size = strlen(S) + extralen + 1;
  char *res = malloc(new_size);
  memset(res, 0, new_size);

  // copio el inicio
  memcpy(res, S, I1);

  // copio lo replicado
  for (int i = 0; i < Q; i++) {
    memcpy(&res[I1 + (I2 - I1 + 1)*i], &S[I1], I2 - I1 + 1);
  }

  // copio el final
  memcpy(&res[I1 + (I2 - I1 + 1)*Q], &S[I2 + 1], strlen(S) - I2);

  return res;
}

Test(test, should_replace) {
  char *res = replicar("Hola", 1, 2, 3);
  printf("Res: %s\n", res);
  char expected[] = "Hololola";
  cr_expect_eq(0, strncmp(expected, res, strlen(expected)));
  free(res);
}
