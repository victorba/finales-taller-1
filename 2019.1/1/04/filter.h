#ifndef __FILTER__H
#define __FILTER__H

#include <stdio.h>

int word_count(const char string[]);

long file_size(FILE *file);

void file_cpy(FILE *file, int dst_offset, int src_offset, int size);

long file_line_count(FILE *file);

void file_filter(const char pathname[]);

#endif
