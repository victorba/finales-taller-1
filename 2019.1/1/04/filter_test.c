#include <criterion/criterion.h>
#include "filter.h"
#include <stdio.h>

Test(filter_test, should_count_words_in_empty_string) {
  char string[] = "";
  cr_expect_eq(0, word_count(string));
}

Test(filter_test, should_count_words_in_non_empty_string) {
  char string[] = "   hola chao tomato tomate";
  cr_expect_eq(4, word_count(string));
}

Test(filter_test, should_return_file_size) {
  FILE *file;
  file = fopen("./input.txt", "r");

  long size = file_size(file);

  cr_expect_eq(78, size);
  fclose(file);
}

Test(filter_test, file_size_should_not_move_file_offset) {
  FILE *file;
  file = fopen("./input.txt", "r");
  long original_offset = ftell(file);

  file_size(file);

  cr_expect_eq(original_offset, ftell(file));
  fclose(file);
}

Test(filter_test, should_copy_file_bytes) {
  system("cp ./input.txt ./input01.txt");
  FILE *file;
  file = fopen("./input01.txt", "r+");
  long size = file_size(file);

  file_cpy(file, 10, 17, size - 17);
  fclose(file);

  cr_expect_eq(
    0,
    system("diff input01.txt output01.txt")
  );
  system("rm input01.txt");
}

Test(filter_test, should_count_lines) {
  FILE *file;
  file = fopen("input.txt", "r");

  long lines = file_line_count(file);
  fclose(file);

  cr_expect_eq(7, lines);
}

Test(filter_test, count_lines_should_not_move_file_offset) {
  FILE *file;
  file = fopen("input.txt", "r");
  long offset = ftell(file);

  file_line_count(file);

  cr_expect_eq(offset, ftell(file));
  fclose(file);
}

Test(filter_test, file_cpy_should_not_move_offset) {
  system("cp ./input.txt ./input02.txt");
  FILE *file;
  file = fopen("./input02.txt", "r+");
  long offset = ftell(file);
  long size = file_size(file);

  file_cpy(file, 10, 17, size - 17);

  cr_expect_eq(offset, ftell(file));
  fclose(file);
  system("rm input02.txt");
}

Test(filter_test, should_filter) {
  system("cp ./input.txt ./input03.txt");

  file_filter("input03.txt");

  cr_expect_eq(
    0,
    system("diff input03.txt output.txt")
  );
}

Test(filter_test, should_get_file_for_command_line) {
  system("cp ./input.txt ./input04.txt");

  system("./main.app input04.txt");

  cr_expect_eq(
    0,
    system("diff input04.txt output.txt")
  );
}
