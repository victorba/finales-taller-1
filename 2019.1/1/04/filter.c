#include "filter.h"
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>


#define WHITESPACE ' '
#define MAXBUFFER 256

enum Status {
  READING,
  WAITING
};

int word_count(const char string[]) {
  enum Status status = WAITING;
  int words = 0;

  for (int i = 0; i < strlen(string); i++) {
    char c = string[i];
    if (c != WHITESPACE && status == WAITING) {
      status = READING;
      words++;
    } else if (c == WHITESPACE && status == READING) {
      status = WAITING;
    }
  }

  return words;
}

long file_size(FILE *file) {
  long prev_offset = ftell(file);
  fseek(file, 0, SEEK_END);
  long size = ftell(file);
  fseek(file, prev_offset, SEEK_SET);
  return size;
}

void file_cpy(FILE *file, int dst_offset, int src_offset, int size) {
  long prev_offset = ftell(file);
  char buffer;

  for (int i = 0; i < size; i++) {
    fseek(file, src_offset, SEEK_SET);
    fread(&buffer, sizeof(buffer), 1, file);
    fseek(file, dst_offset, SEEK_SET);
    fwrite(&buffer, sizeof(buffer), 1, file);
    src_offset += sizeof(buffer);
    dst_offset += sizeof(buffer);
  }

  fseek(file, prev_offset, SEEK_SET);
}

long file_line_count(FILE *file) {
  long prev_offset = ftell(file);

  char buffer[MAXBUFFER + 1];
  long counter = 0;

  while (fgets(buffer, MAXBUFFER, file) != NULL) {
    counter++;
  }

  fseek(file, prev_offset, SEEK_SET);
  return counter;
}

void file_filter(const char pathname[]) {
  FILE *file;
  file = fopen(pathname, "r+");
  long lines = file_line_count(file);
  long size = file_size(file);
  char buffer[MAXBUFFER];
  int offset = 0;
  int short_file_in = 0;

  for (int i = 0; i < lines; i++) {
    fseek(file, offset, SEEK_SET);
    fgets(buffer, MAXBUFFER, file);
    if (word_count(buffer) == 1) {
      file_cpy(
        file,
        offset,
        offset + strlen(buffer),
        size - (offset + strlen(buffer))
      );
      short_file_in += strlen(buffer);
    } else {
      offset += strlen(buffer);
    }
  }

  fclose(file);
  truncate(pathname, size - short_file_in);
}
