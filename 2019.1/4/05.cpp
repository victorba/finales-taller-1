#include <iostream>
#include <list>

template <class T>
bool list_contains(std::list<T> l, T elem) {
  for (T e: l) {
    if (e == elem) {
      return true;
    }
  }
  return false;
}

template <class T>
std::list<T> DobleSegunda(std::list<T> a, std::list<T> b) {
  std::list<T> res;

  for (T e: a) {
    res.push_back(e);
    if (list_contains(b, e)) {
      res.push_back(e);
    }
  }

  return res;
}

int main() {
  std::list<int> x({ 1, 2, 3 });
  std::list<int> y({ 2, 3, 4 });

  std::list<int> z = DobleSegunda(x, y);

  for (int e: z) {
    std::cout << e << std::endl;
  }

  // std::list<int> expected({ 1, 2, 2, 3, 3 }):
  return 0;
}
