# 06 / 8 / 2019

1. ¿Qué es una macro de C? Enumere buenas prácticas para su definición.  
  Una macro es una porcion de codigo con nombre donde luego el preprocesador reemplazara su aparaciciones.
  Buenas practicas son:
  - Usar mayusculas
  - Usar nombres descriptivos
  - Evitar terminarlas en punto y coma```;```, asi no provocar problemas como:
    ```
    #define MAX 3;
    int a = MAX * 5;
    ```
    Que da error de compilacion dado que se traduce como
    ```
    int a = 3; + 5;
    ```
  - Si son function-like, envolver sus parametros en parentesis para evitar errores como:
  ```
  #define MULT(X, Y) X * Y
  ```
  Donde al evaluar con ```MULT(2 + 2 , 3 + 3)``` ejecuta ```2 + 2 * 3 + 3 = 2 + 6 + 3 = 11``` en vez de ```4 * 6 = 24```.
  Utilizando parentesis el problema se evita.
  ```
  MULT(X, Y) ((X) * (Y))
  ```
  Porque ```MULT(2 + 2, 3 + 3) = (2 + 2) * (3 + 3) = 4 * 6 = 24```.
  Aun asi puede ser propensa a errores dado que
  ```
  #define MAX(X, Y) ((X) > (Y) ? (X) : (Y))
  int a = 10;
  int b = 3;
  int c = MAX(++a, b)
  ```
  Se ejecuta como
  ```
  int c = ((++a) > (b) ? (++a) : (b));
  ```
  Incrementando a ```a``` dos veces.

2. Describa el proceso de transformación de código fuente a un ejecutable. Precise las etapas y las tareas desarrolladas en cada una de ellas.   
  1. Precompilacion: Tiene dos etapas
    - Sustitucion y control de la precompilacion:
      Se incluyen en los archivos los headers mediante ```#include``` y se compila condicionalmente mediante ```#ifdef #else #elseif #endif```.
    - Expansion de macros: Se reemplazan function-like macros reemplazando sus simbolos por codigo. Ejemplo: ```#define ADD_TEN(X) ((X) + 10)```.
  2. Compilacion: Se toma el archivo generado en el paso anterior y se obtiene codigo objeto. Tiene tres pasos:
    - Parsing: Se construye un arbol sintactico de simbolos.
    - Traduccion: Se traducen los simbolos a lenguaje ensamblador.
    - Ensamblado: Se traduce el lenguaje ensamblador a codigo objeto. Aca se obtiene codigo especifico para una arquitectura.
  3.Linking: Se resuelven simbolos pendientes del paso anterior. Hay dos tipos:
    - Estatico: Los simbolos se resuelven en tiempo de compilacion con lo cual se incluyen todo el codigo de funciones y variables en el mismo ejecutable.
    - Dinamico: Los simbolos se resuelven en tiempo de ejecucion. El ejecutable es mucho mas pequeño, pero tiene dependencias.

3. Explique breve y concretamente qué es f: ```char (*f) (int *, char[3]);```
  Es un puntero a una funcion que recibe por parametro un puntero a entero y un puntero a un array del cual es deseable que tenga tres elementos del tipo char. Devuelve un char.

4. Escribir un programa ISO C que procese el archivo “nros_2bytes_bigendian.dat” sobre sí mismo, eliminando los número múltiplos de 7.     
  *Ver 04*

5. Implemente una función C++ denominada DobleSegunda que reciba dos listas de elementos y devuelva una nueva lista duplicando los elementos de la primera si están en la segunda: ```std::list<T> DobleSegunda(std::list<T> a,std::list<T> b);```     
  *Ver 05*

6. Escriba un programa que reciba por línea de comandos un Puerto y una IP. El programa debe aceptar una unica conexión e imprimir en stdout todo lo recibido. Al recibir el texto ‘FIN’ debe finalizar el programa sin imprimir dicho texto.
  *Ver 06*

7. Escriba el .H de una biblioteca de funciones ISO C para números complejos. Incluya, al menos, 4 funciones.    
  ```
  // Returns the absolute value of z
  double cabs(double complex z);

  // Returns cosine of z
  double complex ccos(double complex z);

  // Returns exp(z)
  double complex cexp(double complex z);

  // Return log(z)
  double complex clog(double complex z);
  ```

8. Explique qué es cada uno de los siguientes, haciendo referencia a su inicialización, su comportamiento y el area de memoria donde residen:
  - Una variable global static:    
    Se inicia en tiempo de compilacion, solo puede ser accedida desde su mismo modulo y reside en el data segment.

  - Una variable local static:    
    Se inicia en tiempo de compilacion, solo puede ser accedida desde su función y reside en el data segment.

  - Un atributo de clase static:    
    Se inicia en tiempo de compilacion, puede ser accedida por todas las instancias de su clase y reside en el data segment.

  9. ¿Cómo se logra que 2 threads accedan (lectura/escritura) a un mismo recurso compartido sin que se generen problemas de consistencia? Ejemplifique.
    Mediante exclusion mutua usando mutex. Un patron util derivado es el de Monitor, en el cual un objeto encapsula un recurso con un mutex.
    Ejemplo
```
    class Counter {
    private:
      int counter;
      std::mutex m;

    public:
      Counter(int counter): counter(counter) {}

      void inc() {
        std::lock_guard<std::mutex> lock(m);
        counter++;
      }  

      int get_value() {
        std::lock_guard<std::mutex> lock(m);
        return counter;
      }
    };

    void inc_counter(Counter *c) {
      for (int i = 0; i < 100; i++) {
        c->inc();
      }      
    }

    int main() {
      Counter c;
      std::thread x(inc_counter, &c);
      std::thread y(inc_counter, &c);

      x.join();
      y.join();

      std::cout << c.get_value() << std::endl;
      return 0;
    }
```

10. Indique la salida del siguiente programa:
  ```
  class A { A(){cout << “A()” << endl;}
  class B : public A { B(){cout << “B()” << endl;}
  int main () { B b; return 0;}
  ```
  Imprime
  ```
  A()B()
  ```
