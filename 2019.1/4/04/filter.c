#include "filter.h"
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <arpa/inet.h>

long file_size(FILE *file) {
  long prev;
  long size;

  prev = ftell(file);
  fseek(file, 0, SEEK_END);
  size = ftell(file);
  fseek(file, prev, SEEK_SET);
  return size;
}

void file_cpy(FILE *file, long dst_offset, long src_offset, long size) {
  long prev;
  char buffer;

  prev = ftell(file);
  for (long i = 0; i < size; i += sizeof(buffer)) {
    fseek(file, src_offset, SEEK_SET);
    fread(&buffer, sizeof(buffer), 1, file);
    fseek(file, dst_offset, SEEK_SET);
    fwrite(&buffer, sizeof(buffer), 1, file);
    src_offset += sizeof(buffer);
    dst_offset += sizeof(buffer);
  }

  fseek(file, prev, SEEK_SET);
}

void file_filter(const char pathname[]) {
  FILE *file;
  long total_registers;
  long total_size;
  uint16_t buffer;
  long offset;

  file = fopen(pathname, "r+");
  total_size = file_size(file);
  total_registers = total_size / sizeof(buffer);
  offset = 0;

  for (long i = 0; i < total_registers; i++) {
    fseek(file, offset, SEEK_SET);
    fread(&buffer, sizeof(buffer), 1, file);
    buffer = ntohs0(buffer); // ntohs no es ISO, es POSIX
    if (buffer % 7 == 0) {
      file_cpy(
        file,
        offset,
        offset + sizeof(buffer),
        total_size - (offset + sizeof(buffer))
      );
    } else {
      offset += sizeof(buffer);
    }
  }

  // No hay forma de truncar en ISO C, por lo tanto se usa POSIX
  truncate(pathname, offset);
  fclose(file);
}

uint16_t ntohs0(uint16_t bigendian) {
  uint8_t *ptr = (uint8_t *)&bigendian;
  uint8_t result[] = { ptr[1], ptr[0] };
  return *((uint16_t *)result);
}
