#ifndef __FILTER_H__
#define __FILTER_H__

#include <stdio.h>
#include <stdint.h>

long file_size(FILE *file);

void file_cpy(FILE *file, long dst_offset, long src_offset, long size);

void file_filter(const char pathname[]);

uint16_t ntohs0(uint16_t bigendian);

#endif
