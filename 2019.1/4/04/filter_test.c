#include <criterion/criterion.h>
#include "filter.h"
#include <stdint.h>

Test(test, should_filter) {
  system("cp input.dat input01.dat");

  file_filter("input01.dat");

  cr_expect_eq(0, system("diff output.dat input01.dat"));
}

Test(test, should_convert_two_bytes_integer_from_bigendian_to_little_endian) {
  uint8_t bigendian[] = { 0x00, 0x01 };

  uint16_t result = ntohs0(*((uint16_t *)bigendian));

  uint8_t expected[] = { 0x01, 0x00 };
  cr_expect_eq(0, memcmp(&result, expected, sizeof(expected)));
}
