#include "socket.h"
#include "common.h"

int main() {
  socket_t socket;
  const char text[] = "holamundoFINXXXXXXXX";

  socket_client_init(&socket, HOST, PORT);
  socket_send_all(&socket, text, sizeof(text));
  socket_close(&socket);

  return 0;
}
