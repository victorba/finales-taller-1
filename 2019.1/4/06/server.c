#include "common.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>

#define BACKLOG 1

int main() {
  struct addrinfo hints;
  struct addrinfo *res;
  struct addrinfo *ptr;
  int sockfd;
  bool are_we_binded = false;
  int val = 1;

  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE;

  getaddrinfo(HOST, PORT, &hints, &res);

  for (ptr = res; ptr != NULL && !are_we_binded; ptr = ptr->ai_next) {
    sockfd = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
    if (sockfd != -1) {
      setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
      bind(sockfd, ptr->ai_addr, ptr->ai_addrlen);
      are_we_binded = true;
    }
  }

  freeaddrinfo(res);
  if (!are_we_binded) {
    return -1;
  }

  listen(sockfd, BACKLOG);
  int peer = accept(sockfd, NULL, NULL);

  // ejercicio
  int maxbuffer = 3;
  char buffer[maxbuffer];
  bool keep_reading = true;
  int counter = 0;

  while (keep_reading) {
    memcpy(&buffer[0], &buffer[1], maxbuffer - 1);
    recv(peer, &buffer[2], 1, 0);
    if (counter >= 2) { // permite que se llene con 2 char el buffer
      if (memcmp(buffer, "FIN", 3) == 0) {
        keep_reading = false;
      } else {
        printf("c: %c\n", buffer[0]);
      }
    }
    counter++;
  }

  //

  close(peer);
  close(sockfd);

  return 0;
}
