#include <mutex>
#include <thread>
#include <condition_variable>
#include <vector>
#include <iostream>

#define MAX 100

std::mutex m;
std::condition_variable cv;
int counter = 0;
bool is_even = true;

void print_even() {
  while (counter < MAX) {
    std::cout << "even: " << counter << std::endl;
    counter++;
    is_even = false;
    cv.notify_one();
    std::unique_lock<std::mutex> lock(m);
    while (!is_even) {
      cv.wait(lock);
    }
  }
}

void print_odd() {
  while (counter < MAX) {
    std::unique_lock<std::mutex> lock(m);
    while (is_even) {
      cv.wait(lock);
    }
    std::cout << "odd: " << counter << std::endl;
    counter++;
    is_even = true;
    cv.notify_one();
  }
}

int main() {
  std::thread odd(print_odd);
  std::thread even(print_even);

  odd.join();
  even.join();
  return 0;
}
