#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPainter>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *e) {
    QPainter painter(this);
    QPen pen(Qt::red);
    painter.setPen(pen);
    painter.drawLine(QPoint(0, this->height()), QPoint(this->width(), 0));
    painter.drawLine(QPoint(0, 0), QPoint(this->width(), this->height()));
}

