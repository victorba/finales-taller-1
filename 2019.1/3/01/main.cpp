#include <iostream>

// declaracion
class Number {
private:
  int n;

public:
  Number(int n);
  Number operator+(const Number &other);
  void operator++();
  Number operator++(int);
  friend std::ostream &operator<<(std::ostream &out, const Number &number);
  friend std::istream &operator>>(std::istream &in, Number &number);
  operator long();
};

// Definicion: Para que corra el programa
Number::Number(int n): n(n) {}

Number Number::operator+(const Number &other) {
  Number new_number(this->n + other.n);
  return new_number;
}

void Number::operator++() {
  this->n++;
}

Number Number::operator++(int) {
  this->n++;
  return *this;
}

std::ostream &operator<<(std::ostream &out, const Number &number) {
  out << number.n;
  return out;
}

std::istream &operator>>(std::istream &in, Number &number) {
  in >> number.n;
  return in;
}

Number::operator long() {
  return (long) this->n;
}


int main() {
  return 0;
}
