#include <criterion/criterion.h>
#include <string.h>
#include "filter.h"

Test(test, should_filter_words_longer_than_three_in_a_line) {
  char text[] = "la casa";
  char delim[] = " ";
  int maxbuffer = 100;
  char buffer[maxbuffer];
  memset(buffer, 0, maxbuffer);

  filter_words_in_line(buffer, text, delim);

  char expected[] = "la";
  cr_expect_eq(
    0,
    strncmp(buffer, expected, strlen(expected))
  );
}

Test(test, should_filter_words_longer_than_three_in_a_long_line) {
  char text[] = "la casa es grande y brillante";
  char delim[] = " ";
  int maxbuffer = 100;
  char buffer[maxbuffer];
  memset(buffer, '\0', maxbuffer);

  filter_words_in_line(buffer, text, delim);

  char expected[] = "la es y";
  cr_expect_eq(
    0,
    strncmp(buffer, expected, strlen(expected))
  );
}

Test(test, should_filter_words_without_an_extra_buffer) {
  char text[] = "la casa es grande y brillante";
  char delim[] = " ";

  filter_words(text, delim);

  char expected[] = "la es y";
  cr_expect_eq(
    0,
    strncmp(text, expected, strlen(expected))
  );
}

Test(test, should_lines_have_equal_size) {
  char text[] = "la casa es grande y brillante";
  char delim[] = " ";

  filter_words(text, delim);

  char expected[] = "la es y";
  cr_expect_eq(strlen(text), strlen(expected));
}

Test(test, should_filter_words_in_file) {
  system("cp input.txt input01.txt");

  file_filter("input01.txt");

  cr_expect_eq(0, system("diff input01.txt output.txt"));
}
