#ifndef __FILTER_H__
#define __FILTER_H__

#include <stdio.h>

void filter_words_in_line(char output[], char input[], const char delim[]);

void filter_words(char text[], const char delim[]);

void file_cpy(FILE *file, long dst_offset, long src_offset, long size);

void file_filter(const char pathname[]);

long file_size(FILE *file);

long file_lines(FILE *file);

#endif
