#include "filter.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
 #include <sys/types.h>


void filter_words_in_line(char output[], char input[], const char delim[]) {
  char *token;
  int maxlenword = 3;
  char whitespace = ' ';

  token = strtok(input, delim);
  while (token != NULL) {
    if (strlen(token) <= maxlenword) {
      memcpy(output, token, strlen(token));
      output[strlen(token)] = whitespace;
      output += strlen(token) + 1;
    }
    token = strtok(NULL, delim);
  }
}

void filter_words(char text[], const char delim[]) {
  int original_size = strlen(text);
  char input[original_size];
  char output[original_size];
  memcpy(input, text, original_size);
  memset(output, 0, original_size);

  filter_words_in_line(output, input, delim);

  memcpy(text, output, strlen(output));
  text[strlen(output) - 1] = '\0';
}

void file_cpy(FILE *file, long dst_offset, long src_offset, long size) {
  long prev;
  char buffer;

  prev = ftell(file);
  for (long i = 0; i < size; i += sizeof(buffer)) {
    fseek(file, src_offset, SEEK_SET);
    fread(&buffer, sizeof(buffer), 1, file);
    fseek(file, dst_offset, SEEK_SET);
    fwrite(&buffer, sizeof(buffer), 1, file);
    src_offset += sizeof(buffer);
    dst_offset += sizeof(buffer);
  }

  fseek(file, prev, SEEK_SET);
}


long file_size(FILE *file) {
  long prev;
  long size;

  prev = ftell(file);
  fseek(file, 0, SEEK_END);
  size = ftell(file);
  fseek(file, prev, SEEK_SET);

  return size;
}

long file_lines(FILE *file) {
  long prev;
  long lines;
  int maxbuffer = 255;
  char buffer[maxbuffer];

  prev = ftell(file);
  rewind(file);
  lines = 0;
  while (feof(file) == 0) {
    lines++;
    fgets(buffer, maxbuffer, file);
  }
  fseek(file, prev, SEEK_SET);

  return lines;
}
void file_filter(const char pathname[]) {
  FILE *file;
  long total_lines;
  long total_size;
  int maxbuffer = 255;
  char buffer[maxbuffer];
  char delim[] = " ";
  long offset = 0;
  int old_line_size;
  int new_line_size;
  int final_size;

  file = fopen(pathname, "r+");
  total_size = file_size(file);
  total_lines = file_lines(file);
  final_size = total_size;

  printf("total size: %ld\n", total_size);
  printf("total lines: %ld\n", total_lines);

  for (long i = 0; i < total_lines; i++) {
    fseek(file, offset, SEEK_SET);
    fgets(buffer, maxbuffer, file);
    printf("buffer before: %s\n", buffer);
    old_line_size = strlen(buffer);
    filter_words(buffer, delim);
    printf("buffer after: %s\n", buffer);
    new_line_size = strlen(buffer);
    buffer[new_line_size] = '\n';
    new_line_size++;
    printf("old (%d)- new (%d): %d\n", old_line_size, new_line_size, old_line_size - new_line_size);
    final_size -= (old_line_size - new_line_size);
    fseek(file, offset, SEEK_SET);
    fwrite(buffer, new_line_size, 1, file);
    file_cpy(
      file,
      new_line_size,
      old_line_size,
      total_size - old_line_size
    );
    offset += new_line_size;
  }

  printf("final size %d\n", final_size);
  truncate(pathname, final_size);
  fflush(file);
  fclose(file);
}
