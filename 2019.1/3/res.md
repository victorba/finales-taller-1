# 23 / 7 / 2019

1. Declare una clase a elección considerando:
  - Atributos que son necesarios
  - Accesibilidad a la Clase
  - Incluir los operadores +, ++ (post-incremento), --(pre-incremento), >> (impresión), << (carga desde consola), long
  *ver 01.h*

2. ¿Qué es un functor? ¿Qué ventaja ofrece frente a una función convencional? Ejemplifique.   
  Un functor es un objeto que puede ser invocado como una funcion dado que responde al metodo (). Presenta la ventaja de tener estado y por lo tanto evita tener que usar valores hardcodeados o variables globales como en una funcion.
  ```
  class Add {
  private:
    int base;
  public:
    Add(int base): base(base) {}
    int operator(int x) { return base + x; }
  };

  int main() {
    Add add_ten(10);
    // prints 60
    std::cout << add_ten(50) << std::endl;
    return 0;
  }
  ```

3. Escriba un programa que imprima por salida estándar los números entre 1 y 100, en orden ascendente. Se pide que los números sean contabilizados por una variable global única y que los pares sean escritos por un hilo mientras que los impares sean escritos por otro. Contemple la correcta sincronización entre hilos y la liberación de los recursos utilizados.   
  *ver 03*

4. Explique qué es (a), (b), (c) y (d), haciendo referencia a su valor y momento de inicialización, su comportamiento y el area de memoria donde residen:
  ```
  static int a;
  int b()
  {
  static int c; char d=65;
  return c+(int) d;
  }
  ```

  - (a) es una variable entera global que al ser estatico solo puede ser accedida desde dentro del modulo. Se inicia en tiempo de compilacion y reside en el data segment. No tiene valor.
  - (b) es una funcion, puede ser accedida de cualquier modulo, se inicializa en tiempo de compilacion y reside en el code segment. Su valor es un puntero al inicio del codigo de la funcion.
  - (c) es una variable entera estatica y solo puede ser accedida desde la funcion (b). Al ser estatica es inicializada en tiempo de compilcacion y reside en el data segment. No tiene valor.
  - (d) es una variable char de valor 65. Se inicializa en tiempo de ejecucion cada vez que se invoca a la funcion (b). Solo puede ser accedida desde (b) y reside en el stack.

5. Escriba una rutina que dibuje las dos diagonales de la pantalla en color rojo.    
  *ver 05*
  ```
  void MainWindow::paintEvent(QPaintEvent *e) {
    QPainter painter(this);
    QPen pen(Qt::red);
    painter.setPen(pen);
    painter.drawLine(QPoint(0, this->height()), QPoint(this->width(), 0));
    painter.drawLine(QPoint(0, 0), QPoint(this->width(), this->height()));
  }
  ```
