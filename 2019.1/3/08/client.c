#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>

#define NODE "localhost"
#define SERVICE "4444"
#define MAXBUFFER 10

int main() {
  struct addrinfo hints;
  struct addrinfo *res;
  struct addrinfo *ptr;
  bool are_we_connected = false;
  int sockfd;

  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = 0;

  getaddrinfo(NODE, SERVICE, &hints, &res);

  for (ptr = res; ptr != NULL && !are_we_connected; ptr = ptr->ai_next) {
    sockfd = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
    if (sockfd != -1) {
      if (connect(sockfd, ptr->ai_addr, ptr->ai_addrlen) != -1) {
        are_we_connected = true;
      }
    }
  }

  freeaddrinfo(res);
  if (!are_we_connected) {
    return -1;
  }

  // Inicio del ejercicio
  bool keep_going = true;
  bool keep_reading_pack = true;
  char c;
  int total;
  char buffer[MAXBUFFER];
  int i;
  int num;

  while (keep_going) {
    keep_reading_pack = true;
    while (keep_reading_pack) {
      recv(sockfd, &c, sizeof(c), 0);
      printf("c: %c\n", c);
      switch (c) {
        case '[':
          total = 0;
          i = 0;
          memset(buffer, 0, MAXBUFFER);
          break;
        case '+':
          buffer[i] = '\0';
          num = atoi(buffer);
          total += num;
          i = 0;
          break;
        case ']':
          if (i == 0) {
            keep_going = false;
          } else {
            buffer[i] = '\0';
            num = atoi(buffer);
            total += num;
            i = 0;
            printf("total: %d\n", total);
          }
          keep_reading_pack = false;
          break;
        default:
          buffer[i] = c;
          i++;
          break;
      }
    }
  }


  // Fin del ejercicios
  close(sockfd);

  return 0;
}
