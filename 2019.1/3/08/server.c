#include "socket.h"

#define NODE "localhost"
#define SERVICE "4444"
#define BACKLOG 10

int main() {
  socket_t server;
  socket_t peer;
  char buffer[3];

  socket_server_init(&server, SERVICE);
  socket_listen(&server, BACKLOG);
  socket_accept(&server, &peer);

  char text[] = "[1+2][100+311][]";
  socket_send_all(&peer, text, sizeof(text));

  socket_close(&peer);
  socket_close(&server);
  return 0;
}
