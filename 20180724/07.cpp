// ejemplo

#include <iostream>

class AddX {
private:
    int x;

public:
    AddX(int x): x(x) {}
    int operator() (int y) {return this->x + y;}
};

int main() {
    AddX add_ten(10);
    int n = add_ten(2);
    std::cout << n << std::endl;
    return 0;
}
