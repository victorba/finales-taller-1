// Escriba un programa que escriba por salida estandar los numeros entre
// 0 y 1000 ordenadamente. Se pide que los numeros pares sean escritos por un
// hilo mientras los impares sean escritos por otro.
// Contemple la correcta sincronizacion entre los hilos y la liberacion de los
// recursos utilizados.
// Detalle

#include <thread>
#include <iostream>
#include <mutex>
#include <condition_variable>

#define MAX_NUMBER 10

std::mutex m;
std::condition_variable cv;
unsigned int number = 0;

bool is_even(unsigned int n) {
    return n % 2 == 0;
}

bool is_odd(unsigned int n) {
    return !is_even(n);
}

void odd_thread() {
    while (number < MAX_NUMBER) {
        std::unique_lock<std::mutex> lk(m);
        cv.wait(lk, []{return is_odd(number);});
        std::cout << "odd thread: " << number << std::endl;
        number++;
        cv.notify_one();
    }
}

void even_thread() {
    while (number < MAX_NUMBER) {
        std::unique_lock<std::mutex> lk(m);
        cv.wait(lk, []{return is_even(number);});
        std::cout << "even thread: " << number << std::endl;
        number++;
        cv.notify_one();
    }
}

int main() {
    std::thread even(even_thread);
    std::thread odd(odd_thread);

    even.join();
    odd.join();

    return 0;
}
