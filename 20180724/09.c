// Escriba un programa C que reciba por argumento el nombre de un archivo
// de numeros binarios de 16 bits y lo procese sobre si mismo.
// El procesamiento consiste en repetir los numeros que sean "multiplos de 5 + 1" (6, 11, 16...)
// (El archivo se agranda)

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>

#define PATHNAME "09-file.bin"

int main() {
    FILE *file;
    uint16_t n = 0;
    unsigned int additional_bytes_needed = 0;
    int old_size;
    int new_size;

    // Abro el archivo
    if ((file = fopen(PATHNAME, "r+")) == NULL) {
        perror("error fopen");
        fclose(file);
    }

    // Calculo la cantidad de bytes adicionales necesarios
    while (fread(&n, sizeof(n), 1, file) == 1) {
        if ((n - 1) % 5 == 0) {
            additional_bytes_needed += sizeof(n);
        }
    }

    // calculo la cantidad de bytes del archivo original
    fseek(file, 0, SEEK_END);
    old_size = ftell(file);
    new_size = old_size + additional_bytes_needed;

    // extiendo el archivo en los bytes necesitados
    for (int i = 0; i < additional_bytes_needed/sizeof(n); i++) {
        fwrite(&n, sizeof(n), 1, file);
    }

    // Posiciono los indices
    int j = new_size - sizeof(n);
    for (int i = old_size - sizeof(n); i >= 0; i -= sizeof(n)) {
        fseek(file, i, SEEK_SET);
        fread(&n, sizeof(n), 1, file);
        fseek(file, j, SEEK_SET);
        fwrite(&n, sizeof(n), 1, file);
        j -= sizeof(n);

        if ((n - 1) % 5 == 0) {
            fseek(file, j, SEEK_SET);
            fwrite(&n, sizeof(n), 1, file);
            j -= sizeof(n);
        }
    }


    printf("additional_bytes_needed: %u\n", additional_bytes_needed);
    fclose(file);
    return 0;
}
