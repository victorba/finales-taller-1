// Defina la clase URL para permitir almacenar las siguientes propiedades:
// protocolo (http), host (fi.uba.ar), port (80), y file (index.php o resources/img/logo.png)
// A su vez se pide que implemente los siguientes operadores: operator<<, operator==
// y operator=.

// ????????????????????????

#include <string>

class URL {
private:
    std::string protocolo;
    std::string host;
    std::string port;
    std::string file;

public:
    URL(const std::string &protocolo,
        const std::string &host,
        const std::string &port,
        const std::string &file);
};


URL::URL(
    const std::string &protocolo,
    const std::string &host,
    const std::string &port,
    const std::string &file):
    protocolo(protocolo),
    host(host),
    port(port),
    file(file) {
}
