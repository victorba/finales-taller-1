5) Explique que es y para que sirve un constructor MOVE en C++. Indique como
se comporta el sistema si este no es definido por el desarrollador.

Un constructor por movimiento crea un objeto y obtiene el ownership de los datos de otro dejandolo en un estado valido. 

Son utiles porque son mas rapidos que los constructores por copia y utilizan menos recursos dado que no es necesario copiar todos los elementos de un objeto a otro, sino que solo se transfiere el ownership.

Si no esta definido entonces el sistema le creara uno si y solo si 
- no tiene un constructor por copia definido
- no tiene un constructor por asignacion definido
- no tiene un constructor por asignacion de movimiento definido
- no tiene definido destructores
- el constructor por movimiento no esta declarado como deleted.

En dicho caso se movera cada uno de los atributos del objeto al nuevo.
