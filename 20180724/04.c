// Escriba una funcion C llamada Agrandar que reciba por parametro 1 cadena (S),
// dos indices (I1, I2) y una cantidad (Q).
// La funcion debe retornar una copia de S salvo los caracteres entre los indices
// I1 y I2 que seran duplicados Q veces en la misma posicion.
// Ej: Agrandar("Chau", 1, 2, 3) retorna "Chahahau"

// Must be freed by user.

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char *agrandar(char S[], int I1, int I2, int Q);

int main() {
    char *r = agrandar("Chau", 1, 2, 3);
    printf("%s\n", r);
    free(r);
}

char *agrandar(char S[], int I1, int I2, int Q) {
    int bytes_needed = strlen(S) + (I2 - I1 + 1) * (Q - 1) + 1;
    char *r = (char *)malloc(bytes_needed);
    r[bytes_needed] = '\0';

    // copio lo de antes del duplicado
    memcpy(r, S, I1);

    // copio lo duplicado
    char *ptr = &r[I1];
    int len = I2 - I1 + 1;
    for (int i = 0; i < Q; i++) {
        memcpy(ptr, &S[I1], len);
        ptr += len;
    }

    // copio lo que queda
    memcpy(ptr, &S[I2 + 1], strlen(S) - I2);
    return r;
}
