// test

#include <stdio.h>

int ten() {
    return 10;
}

// Definicion de funcion B que recibe a 'a' unsigned int y b short.
// Devuelve un puntero a char.
// No tiene codigo.
// char *B(unsigned a, short b) {
// }

int main() {
    // puntero a funcion sin parametros que devuelve un int.
    int (*A)();
    A = &ten;
    printf("A: %d\n", A());

    // Array C en data segment de tamanio 3 de punteros a punteros a unsigned int.
    static unsigned **C[3];
    unsigned int x = 99;
    unsigned int *ptr = &x;
    C[0] = &ptr;
    C[1] = &ptr;
    C[2] = &ptr;
    printf("**C[0]: %u\n", **C[0]);
    printf("**C[1]: %u\n", **C[1]);
    printf("**C[2]: %u\n", **C[2]);


    return 0;
}
