// Defina un rutina en C que se conecte a la IP 10.9.8.7, puerto 7777 y procese
// la informacion recibida. El proceso consiste en recibir textos numericos
// utilizando '\n' como caracter delimitador.
// Para cada entero recibido se debe enviar su valor convertido en 32 bits
// big-endian en modo binario sin delimitadores. El proceso finaliza al recibir
// el valor 0.

#include "socket.h"
#include <stdint.h>
#include <arpa/inet.h>
#include <stdlib.h>

// Idea:
// Crear tda socket0 que tenga
// 1. socket0_init()
// 2. socket0_recv_all_unit_char() que termina de recibir hasta un caracter
//    y devuelve cero si se corto la connexion (va leyendo de a uno y si
//    encuentra el char termina y devuelve cuanto leyo)
// 3. socket0_send_all() el tipico que envia un tamanio.

//
// socket0_init(skt);
// char buf[MAX];
// char end_connection_char = '\n';
// while (socket0_recv_all_until_char(skt, buf, MAX, end_connection_char) > 0) {
//     int32_t number = atoi(buf);
//     number = htonl(number);
//     socket0_send_all(skt, number, sizeof(uint32_t));
// }
// socket0_close(skt);

#define NODE "localhost"
#define SERVICE "4545"
#define MAX 15

typedef struct __socket0_t {
    int sockfd;
} socket0_t;

// Initializes a client socket
// Returns 0 on success or -1 on error.
int socket0_client_init(
    socket0_t *skt,
    const char node[],
    const char service[]);

// Receives data in a buffer until reach the del character
// Buf must be large enought to contain the received message.
// Returns -1 on error, 0 if the connection was closed or the bytes received.
int socket0_recv_all_until_char(socket0_t *skt, char buf[], char del);

// Sends the whole contest of buf.
// Returns the bytes sent or 0 if the connection was closed or -1 on error.
int socket0_send_all(socket0_t *skt, char buf[], int size);

// Closes a socket
int socket0_close(socket0_t *skt);

// Conecta y envia los datos de prueba.
void test02();

int main(int argc, char *argv[]) {
    if (argc > 1) {
        test02();
        return 0;
    }

    int r;
    char buf[MAX];
    socket0_t skt;

    if ((r = socket0_client_init(&skt, NODE, SERVICE)) == -1) {
        printf("Error socket init\n");
        return -1;
    }

    memset(buf, 0, MAX);
    while ((r = socket0_recv_all_until_char(&skt, buf, '\n')) > 0) {
        buf[r] = '\0';
        printf("buffer: %s\n", buf);
        int32_t integer = atoi(buf);
        printf("after atoi: %d\n", integer);
        uint32_t uinteger = htonl((uint32_t)integer);
        socket_send_all((socket_t *)&skt, (char *)&uinteger, sizeof(uinteger));
    }

    socket0_close(&skt);
    return 0;
}

int socket0_client_init(
    socket0_t *skt,
    const char node[],
    const char service[]) {
    struct addrinfo hints;
    struct addrinfo *res;
    struct addrinfo *ptr;
    bool are_we_connected = false;
    int r;
    int s;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = 0;

    if ((r = getaddrinfo(node, service, &hints, &res)) < 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(r));
        return -1;
    }

    for (ptr = res; ptr != NULL && !are_we_connected; ptr = ptr->ai_next) {
        s = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
        if (s != -1) {
            r = connect(s, ptr->ai_addr, ptr->ai_addrlen);
            if (r != -1) {
                are_we_connected = true;
            }
        }
    }

    freeaddrinfo(res);

    if (!are_we_connected) {
        return -1;
    }

    skt->sockfd = s;
    return 0;
}

int socket0_recv_all_until_char(socket0_t *skt, char buf[], char del) {
    int received = 0;
    int r;
    bool del_char_reached = false;
    bool there_was_an_error = false;
    bool closed_connection = false;


    while (!del_char_reached && !there_was_an_error && !closed_connection) {
        r = recv(skt->sockfd, &buf[received], 1, 0);
        if (r == -1) {
            there_was_an_error = true;
        } else if (r == 0) {
            closed_connection = true;
        } else if (buf[received] == del) {
            del_char_reached = true;
        } else {
            received += r;
        }
    }

    if (there_was_an_error) {
        return -1;
    } else if (closed_connection) {
        return 0;
    } else {
        return received;
    }
}

int socket0_close(socket0_t *skt) {
    return close(skt->sockfd);
}

int socket0_send_all(socket0_t *skt, char buf[], int size) {
    bool there_was_an_error = false;
    bool closed_connection = false;
    int sended = 0;
    int r;

    while (sended < size && !there_was_an_error && !closed_connection) {
        r = send(skt->sockfd, &buf[size], size - sended, MSG_NOSIGNAL);
        if (r == -1) {
            there_was_an_error = true;
        } else if (r == 0) {
            closed_connection = true;
        } else {
            sended += r;
        }
    }

    if (there_was_an_error) {
        return -1;
    } else {
        return sended;
    }

    return 0;
}


void test02() {
    socket_t sv;
    socket_server_init(&sv, SERVICE);
    socket_listen(&sv, 500);

    socket_t peer;
    socket_accept(&sv, &peer);

    char buf[] = {'1', '2', '\n', '3', '4', '\n'};
    socket_send_all(&peer, buf, sizeof(buf));
    socket_shutdown(&peer, SHUT_WR);

    int r;
    uint32_t integer;
    while ((r = socket_recv_all(&peer, (char *)&integer, sizeof(integer))) > 0) {
        integer = ntohl(integer);
        printf("integer: %u\n", integer);
    }

    socket_close(&peer);
    socket_close(&sv);
}
