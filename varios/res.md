# Varios ejercicios no vistos en otros finales

1. **FINAL: 12 / 2 / 2019 ej 6:** Escriba una función ISO C que permita procesar un archivo texto sobre sí mismo, que contenga una palabra por línea. El procesamiento consiste en ordenar las palabras (líneas) alfabéticamente considerando que el archivo no entra en memoria.     
  *Ver 001*

2. Defina una macro llamada EXEC_IN_POSIX que tome un puntero a función. La macro  debe verificar si existe una versión definida de POSIX durante la compilación y ejecutar el puntero a función en dicho caso. Para detectar si existe una versión de POSIX se debe verificar la existencia de la macro ```_POSIX_VERSION```, provista por el compilador.
  *Ver 002*

3. Escriba una rutina que muestre un botón y un cuadro de texto en una ventana. Al hacer click en el botón debe desaparecer el cuadro de texto, dejando el resto de la ventana intácta.

4. ¿Para qué sirve un constructor privado? Ejemplifique.    
  Un constructor privado puede ser util cuando:
  - Se tiene una clase A amiga de otra B y se desea que solo esa clase A puede instanciar objetos de B.
  - Singleton Pattern: Solo puede haber una instancia del objeto.
  - Idiomatico: En vez de construir a un objeto invocando su mismo nombre (ej: clase Casa se instancia como Casa()) se podria hacer privado a Casa() y que solo se construya mediante (Casa::new()) siguiendo un estilo mas parecido a rust.

5. Suponga que dos aplicaciones (A y B) se encuentran comunicadas por un socket TCP. La aplicación A envía a la B un paquete de 1023 bytes. ¿Como puede recibir la aplicación B esos datos? Juntos?...Partidos en 4?.. en 3?....    
  Puede recibirlos partidos en diferentes tamaños pero por lo general TCP envia segmentos de tamaño MSS de 1500 bytes aunque por capa de networking estos pueden ser fragmentados en paquetes mas pequeños aunque siempre deben ser multiplos de 8.

6. Dibujar un circulo azul del tamaño de la ventana.
  *Ver 006*

7. 
