#include "filter.h"
#include <criterion/criterion.h>
#include <stdio.h>

Test(test, should_swap_lines_in_file) {
  system("cp input_swap_00.txt input_swap_00_test.txt");
  FILE *file;
  file = fopen("input_swap_00_test.txt", "r+");

  file_swap(file, 0, 3, 7, 7);

  fclose(file);
  cr_expect_eq(0, system("diff input_swap_00_test.txt output_swap_00.txt"));
  system("rm input_swap_00_test.txt");
}

Test(test, should_swap_lines_in_file_when_first_is_larger) {
  system("cp input_swap_01.txt input_swap_01_test.txt");
  FILE *file;
  file = fopen("input_swap_01_test.txt", "r+");

  file_swap(file, 0, 7, 11, 3);

  fclose(file);
  cr_expect_eq(0, system("diff input_swap_01_test.txt output_swap_01.txt"));
  system("rm input_swap_01_test.txt");
}

Test(test, should_sort_words) {
  system("cp input_sort_00.txt input_sort_00_test.txt");

  file_sort("input_sort_00_test.txt");

  cr_expect_eq(0, system("diff input_sort_00_test.txt output_sort_00.txt"));
}
