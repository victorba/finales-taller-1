#include "filter.h"
#include <stdio.h>
#include <string.h>

#define MAXBUFFER 255

void file_cpy(FILE *file, long dst_offset, long src_offset, long size) {
  long prev;
  char buffer;

  prev = ftell(file);
  for (long i = 0; i < size; i++) {
    fseek(file, src_offset, SEEK_SET);
    fread(&buffer, sizeof(buffer), 1, file);
    fseek(file, dst_offset, SEEK_SET);
    fwrite(&buffer, sizeof(buffer), 1, file);
    dst_offset++;
    src_offset++;
  }

  fseek(file, prev, SEEK_SET);
}

void file_swap(FILE *file, long vx, long sizex, long vy, long sizey) {
  long prev;
  long vm;
  long sizem;
  char bufferx[MAXBUFFER];
  char buffery[MAXBUFFER];

  prev = ftell(file);
  memset(bufferx, 0, MAXBUFFER);
  memset(buffery, 0, MAXBUFFER);

  // read in buffers
  fseek(file, vy, SEEK_SET);
  fread(buffery, sizey, 1, file);
  fseek(file, vx, SEEK_SET);
  fread(bufferx, sizex, 1, file);

  // Move inbetween segment
  vm = vx + sizex;
  sizem = vy - vm;
  file_cpy(file, vx + sizey, vm, sizem);

  // writing
  fseek(file, vx, SEEK_SET);
  fwrite(buffery, sizey, 1, file);
  fseek(file, vy + (sizey - sizex), SEEK_SET);
  fwrite(bufferx, sizex, 1, file);

  fseek(file, prev, SEEK_SET);
}

long file_size(FILE *file) {
  long prev;
  long size;

  prev = ftell(file);
  fseek(file, 0, SEEK_END);
  size = ftell(file);
  fseek(file, prev, SEEK_SET);

  return size;
}

void file_sort(const char pathname[]) {
  FILE *file;
  long i;
  long j;
  long min;
  long size;
  char buffer_i[MAXBUFFER];
  char buffer_j[MAXBUFFER];
  char buffer_min[MAXBUFFER];

  file = fopen(pathname, "r+");
  size = file_size(file);
  memset(buffer_i, 0, MAXBUFFER);
  memset(buffer_j, 0, MAXBUFFER);
  memset(buffer_min, 0, MAXBUFFER);
  i = 0;
  j = 0;
  min = 0;

  while (i < size) {
    fseek(file, i, SEEK_SET);
    fgets(buffer_i, MAXBUFFER, file);
    memcpy(buffer_min, buffer_i, MAXBUFFER);
    min = i;
    j = ftell(file);
    while (j < size) {
      fgets(buffer_j, MAXBUFFER, file);
      if (strncmp(buffer_min, buffer_j, MAXBUFFER) > 0) {
        min = j;
        memcpy(buffer_min, buffer_j, MAXBUFFER);
      }
      j = ftell(file);
    }
    if (min != i) {
      file_swap(file, i, strlen(buffer_i), min, strlen(buffer_min));
    }
    i += strlen(buffer_i);
  }

  fclose(file);
}
