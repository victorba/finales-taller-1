#ifndef __FILTER_H__
#define __FILTER_H__

#include <stdio.h>
#include <stdlib.h>

void file_cpy(FILE *file, long dst_offset, long src_offset, long size);

// Pre: vx < vy
void file_swap(FILE *file, long vx, long sizex, long vy, long sizey);

long file_size(FILE *file);

void file_sort(const char pathname[]);


#endif
