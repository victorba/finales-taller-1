#include "mainwindow.h"

#include <QApplication>
#include <QLineEdit>
#include <QBoxLayout>
#include <QObject>
#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    QPushButton *button = new QPushButton("Press me", &w);
    QLineEdit *edit = new QLineEdit(&w);
    w.show();
    button->move(100, 100);
    edit->move(100, 200);
    button->show();
    edit->show();
    QObject::connect(button, SIGNAL(clicked(bool)), edit, SLOT(hide()));
    return a.exec();
}
