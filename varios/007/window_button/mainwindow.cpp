#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPushButton>
#include <QBoxLayout>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::MainWindow),
      button(new QPushButton("button"))
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

