#include <stdio.h>
#include <unistd.h>

#ifdef _POSIX_VERSION
#define EXEC_IN_POSIX(F) F()
#endif

int main() {
  EXEC_IN_POSIX(fork);
  return 0;
}
