#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPainter>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *e) {
    QPainter painter(this);
    QBrush brush(Qt::blue);
    painter.setBrush(brush);
    painter.drawEllipse(QPoint(this->width() / 2, this->height() / 2), this->width() / 2, this->height() / 2);
}

