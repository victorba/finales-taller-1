# 11 / 12 / 2018

1. ¿Por qué las clases que utilizan templates se declaran y definen en los .h?     
  Porque cuando un compilador encuentra que se esta utilizando un template instancia (en caso de encontrarse) una clase/funcion para un cierto tipo y para poder compilarla necesita tanto de su declaracion como definicion. Entonces, si no estuviese su definicion en el .h, el compilador no podria generar codigo objeto al faltar su definicion.

2. ¿Qué es un functor? Ejemplifique.   
  Es un objeto que responde al metodo ```()``` y tiene un comportamiento parecido al de una funcion. Dado que es un objeto, tiene la ventaja sobre las funciones de manejar estado mediante metodos/atributos en vez de una variable static. Ejemplo:
  ```
  class Adder {
    int n;
    Adder(int n): n(n) {}
    int operator() (int x) { return n + x; }
  };

  int main() {
    Adder add_ten(10);
    assert(15, add_ten(15));
    assert(110, add_ten(100));
    return 0;
  }

  ```

3. Describa con exactitud las siguientes declaraciones/definiciones globales:
  ```
  extern float (*I)[3];
  static int *C[3];
  static short F(const float *a);
  ```
  - ```extern float (*I)[3];```   
    I es una declaracion de un puntero a un array de 3 elementos del tipo float.
  - ```static int *C[3];```   
    C es una definicion de un array de punteros del tipo int. Es estatico por lo cual solo puede ser accedido desde el mismo modulo y se inicializa en el data segment.
  - ```static short F(const float *a);```     
    F es una declaracion de una funcion que recibe por parametros a un puntero del tipo float constante y devuelve un short int. Al ser estatica solo puede ser accedida desde su mismo modulo.


4. ¿Qué es un Deadlock? Ejemplifique.    
  Un Deadlock es un estado en el cual un conjunto de procesos o hilos esta esperando por la liberacion de un recurso el cual es controlado por otro proceso (o incluso ellos mismos).
  Ejemplo: *ver 04*

5. Explique qué es y para qué sirve una variable de clase (o atributo estático) en C++. Mediante un ejemplo de uso, indique cómo se define dicha variable, su inicialización y el acceso a su valor para realizar una impresión simple dentro de un main.     
  Los atributos estaticos son variables compartidas por todas las instancias de la clase y sirven para compartir estado. *Ver 05*

6. ¿Qué significa que una función es blocante?¿Cómo subsanaría esa limitación en término de mantener el programa ‘vivo’ ?      
  Una funcion es bloqueante cuando su ejecucion se detiene hasta la ocurrencia de un evento (ej: I/O como recv en un socket). Para subsanarla se puede invocar dicha funcion en un hilo aparte del principal.

7. Explique qué es y para qué sirve un constructor MOVE en C++. Indique cómo se comporta el sistema si éste no es definido por el desarrollador.    
  Un constructor move es un constructor que permite instanciar un objeto entregandole el ownership de recursos de otro (del que esta basado) y sin generar copias. Se utiliza cuando se necesita transferir ownership de recursos entre objetos. Ej: Cuando se devuelve un objeto de una funcion y se desea que un objeto externo tenga el ownership de un recurso y que no sea liberado tras la destruccion del objeto local de la funcion.
  Si no esta definido, sse utiliza el constructor move default que mueve cada recurso al nuevo objeto.

8. Escribir un programa ISO C que procese el archivo “valuesword.dat” sobre sí mismo, eliminando los words (2 bytes) múltiplos de 16.    
  *Ver 08*

9. Implemente la función ```void ValorHex(char *hex, int *ent)``` que interprete la cadena hex (de símbolos hexadecimales) y guarde el valor correspondiente en el entero indicado por ent.     
  *Ver 09*

10. Implemente una función C++ denominada Interseccion que reciba dos listas de elementos y devuelva una nueva lista con los elementos que se encuentran en ambas listas: ```std::list<T> Interseccion(std::list<T> a,std::list<T> b);```
  *Ver 10.cpp*
