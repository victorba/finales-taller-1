#ifndef __FILTER_H__
#define __FILTER_H__

#include <stdint.h> // ES ISO
#include <stdio.h>

// NO ISO, pero necesario para truncar
#include <unistd.h>
#include <sys/types.h>


uint16_t ntohs0(uint16_t n);

long file_size(FILE *file);

void file_cpy(FILE *file, long dst_offset, long src_offset, long size);

void file_filter(const char pathname[]);


#endif
