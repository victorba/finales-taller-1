#include "filter.h"
#include <stdint.h>
#include <stdio.h>

uint16_t ntohs0(uint16_t n) {
  uint8_t *ptr = (uint8_t *)&n;
  uint8_t res[] = { ptr[1], ptr[0] };
  return *((uint16_t *)res);
}

long file_size(FILE *file) {
  long prev;
  long size;

  prev = ftell(file);
  fseek(file, 0, SEEK_END);
  size = ftell(file);
  fseek(file, prev, SEEK_SET);

  return size;
}

void file_cpy(FILE *file, long dst_offset, long src_offset, long size) {
  long prev;
  uint8_t buffer;

  prev = ftell(file);
  for (long i = 0; i < size; i += sizeof(buffer)) {
    fseek(file, src_offset, SEEK_SET);
    fread(&buffer, sizeof(buffer), 1, file);
    fseek(file, dst_offset, SEEK_SET);
    fwrite(&buffer, sizeof(buffer), 1, file);
    dst_offset += sizeof(buffer);
    src_offset += sizeof(buffer);
  }

  fseek(file, prev, SEEK_SET);
}

void file_filter(const char pathname[]) {
  FILE *file;
  long size;
  long registers;
  long offset;
  uint16_t buffer;
  uint16_t number;

  file = fopen(pathname, "r+");
  size = file_size(file);
  registers = size / sizeof(buffer);
  offset = 0;

  for (long i = 0; i < registers; i++) {
    fseek(file, offset, SEEK_SET);
    fread(&buffer, sizeof(buffer), 1, file);
    number = ntohs0(buffer);
    if (number % 16 == 0) {
      file_cpy(
        file,
        offset,
        offset + sizeof(buffer),
        size - (offset + sizeof(buffer))
      );
    } else {
      offset += sizeof(buffer);
    }
  }

  truncate(pathname, offset);
  fclose(file);
}
