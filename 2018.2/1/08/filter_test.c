#include "filter.h"
#include <criterion/criterion.h>
#include <stdint.h>

Test(test, should_convert_big_endian_to_little_endian) {
  uint8_t bigendian[] = { 0x00, 0x01 };

  uint16_t res = ntohs0(*((uint16_t *) bigendian));

  uint8_t expected[] = { 0x01, 0x00 };
  cr_expect_eq(0, memcmp(&res, expected, sizeof(expected)));
}

Test(test, should_tell_file_size) {
  FILE *file;
  long size;
  long expected_size;

  file = fopen("valuesword.dat", "r");
  size = file_size(file);
  fclose(file);

  expected_size = 8;
  cr_expect_eq(expected_size, size);
}

Test(test, should_filter_file) {
  system("cp valuesword.dat input.dat");
  file_filter("input.dat");
  cr_expect_eq(0, system("diff input.dat output.dat"));
}
