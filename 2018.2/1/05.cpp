#include <iostream>

// Ejemplo: Contador de instancias de la clase.

class Counter {
public:
  // Se define anteponiendo la palabre static
  static int n;
  Counter() { n++; }
  ~Counter() { n--; }
  int get_value() { return n; }
};

// Inicializacion fuera de la clase
int Counter::n = 0;

int main() {
  int max = 10;
  Counter *x = new Counter[max];

  // Se puede acceder mediante un metodo de uno de sus instancias
  std::cout << "Instances of Counter: "
            << x[0].get_value()
            << std::endl;

  delete []x;

  // Si es publico, tambien se puede acceder mediante la
  // propia clase.
  std::cout << "Instances of Counter: "
            << Counter::n
            << std::endl;

  return 0;
}
