#include <thread>
#include <mutex>

std::mutex red_mutex;
std::mutex blue_mutex;

void red_first() {
  red_mutex.lock();
  blue_mutex.lock();

  blue_mutex.unlock();
  red_mutex.unlock();
}

void blue_first() {
  blue_mutex.lock();
  red_mutex.lock();

  red_mutex.unlock();
  blue_mutex.unlock();
}

// Ambos threads pueden quedar en deadlock si se genera el
// siguiente schedule
//   BLUE               ||  RED
//   blue_mutex.lock()  ||
//                      ||  red_mutex.lock()
//   red_mutex.lock()   ||
//                      ||  blue_mutex.lock()
//

int main() {
  std::thread blue(blue_first);
  std::thread red(red_first);

  blue.join();
  red.join();

  return 0;
}
