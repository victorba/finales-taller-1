#include <criterion/criterion.h>
#include <stdlib.h>
#include <stdio.h>

void valorhex(char *hex, int *ent) {
  int base = 16;
  *ent = (int) strtol(hex, NULL, base);
}

Test(test, should_transform_hex_string_to_int) {
  char string[] = "0xF";
  int res;

  valorhex(string, &res);

  cr_expect_eq(15, res);
}
