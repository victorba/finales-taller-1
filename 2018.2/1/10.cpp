#include <list>
#include <iostream>

template<class T>
bool list_contains(const std::list<T> &l, const T &elem) {
  for (const T e: l) {
    if (e == elem) {
      return true;
    }
  }
  return false;
}

template <class T>
std::list<T> interseccion(std::list<T> a, std::list<T> b) {
  std::list<T> res;

  for (T elem: a) {
    if (list_contains(b, elem)) {
      res.push_back(elem);
    }
  }

  return res;
}

int main() {
  std::list<int> a({ 1, 2, 3 });
  std::list<int> b({ 2, 3, 4, 5 });

  std::list<int> res = interseccion(a, b);

  for (int e: res) {
    std::cout << e << std::endl;
  }

  return 0;
}
