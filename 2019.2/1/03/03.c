#include <criterion/criterion.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>


bool is_divisible(int x, int y) {
  return x % y == 0;
}

int file_size(FILE *file) {
  fseek(file, 0, SEEK_END);
  return ftell(file);
}

void file_copy_right(FILE *file,  int src, int dst) {
  int extra_size = dst - src;
  int read;
  int write;
  int original_size;
  char buffer;

  original_size = file_size(file);
  ftruncate(fileno(file), original_size + extra_size);
  read = original_size - 1;
  write = read + extra_size;

  while (read >= src) {
    fseek(file, read, SEEK_SET);
    fread(&buffer, 1, sizeof(buffer), file);
    fseek(file, write, SEEK_SET);
    fwrite(&buffer, 1, sizeof(buffer), file);
    read--;
    write--;
  }
}

int file_duplicator(char *pathname) {
  FILE *file;
  short int buffer;
  int read;

  file = fopen(pathname, "r+");

  read = 0;
  while (read < file_size(file)) {
    fseek(file, read, SEEK_SET);
    fread(&buffer, 1, sizeof(short int), file);

    if (is_divisible(buffer, 3)) {
      file_copy_right(file, read, read + sizeof(short int));
      read += sizeof(short int);
    }
    read += sizeof(short int);
  }

  fclose(file);
  return 0;
}

Test(duplicator, should_indicate_when_multiple_by_three) {
  cr_assert(is_divisible(6, 3));
}

Test(duplicator, should_return_file_size) {
  FILE *file;
  file = fopen("./03_simple_original.bin", "r");

  cr_assert(10 == file_size(file));
  fclose(file);
}

Test(duplicator, should_copy_file_right) {
  int exit_status;
  FILE *file;
  system("cp ./03_simple_original.bin ./03_simple_test.bin");

  file = fopen("./03_simple_test.bin", "r+");
  file_copy_right(file, 4, 6);

  exit_status = system("cmp ./03_simple_test.bin ./03_simple_copied_right_exp.bin");
  system("rm ./03_simple_test.bin");
  fclose(file);
  cr_assert(exit_status == 0);
}

Test(duplicator, should_duplicate_when_divisible_by_three) {
  system("cp ./03_simple_original.bin ./03_simple_01_test.bin");

  file_duplicator("./03_simple_01_test.bin");

  int exit_status = system("cmp ./03_simple_01_test.bin ./03_simple_exp.bin");
  cr_assert(exit_status == 0);
  system("rm ./03_simple_01_test.bin");
}
