#include <stdio.h>

int (*f) (short int *, char[3]);

int sumar(short int *x, char v[3]) {
	return 3;
}

int main() {
	f = sumar;
	short int x = 5;
	char v[3] = { 'a', 'b', 'c' };
	
	printf("f = %d\n", f(&x, v));
	return 0;
}