#include <mutex>
#include <thread>
#include <iostream>

class Counter {
private:
  int value;
  std::mutex m;

public:
  Counter(): value(0) {}

  void inc() {
    std::lock_guard<std::mutex> guard(this->m);
    this->value++;
  }

  int get_value() {
    std::lock_guard<std::mutex> guard(this->m);
    return this->value;
  }
};

Counter c;

void add() {
  int max = 1000000;
  for (int i = 0; i < max; i++) {
    c.inc();
  }
}

int main() {
  std::thread t1(add);
  std::thread t2(add);

  t1.join();
  t2.join();

  std::cout << "Value: " << c.get_value() << std::endl;
  return 0;
}
