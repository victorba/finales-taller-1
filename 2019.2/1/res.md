# Respuestas

1. Explique breve y concretamente qué es f: ```int (*f) (short int *, char[3]);```
    Es la definición de un puntero a una función que recibe un puntero a ```short int``` y un puntero a ```char``` que tiene un array de 3 elementos. Devuelve un entero con signo.

2. Explique qué es cada uno de los siguientes, haciendo referencia a su inicialización, su
comportamiento y el area de memoria donde residen:  
    a) Una variable global static  
    b) Una variable local static  
    c) Un atributo de clase static.  

    * **Variable global**:
        - Se crea al inicio del programa.
        - Se puede ser accedida desde que se define o declara hasta el final del archivo.            
          Solo puede ser accedida desde el mismo archivo.
        - Reside en el data segment.

    * **Local static variable**
        - Se crea al inicio del programa.
        - Solo puede ser accedida dentro de la función.
        - Reside en el data segment.

    * **Atributo clase static**
        - Se crea al inicio del programa.
        - Solo puede ser accedida por las instancias de la clase.
        - Reside en el data segment.


4. ¿Cómo se logra que 2 threads accedan (lectura/escritura) a un mismo recurso compartido sin que se generen problemas de consistencia? Ejemplifique.  

  Se utilizan mutex (exclusión mutua) para evitar que dos threads accedan a un mismo recurso compartido. Ejemplo en 04_mutex.cpp.

5. Escriba el .H de una biblioteca de funciones ISO C para cadenas de caracteres. Incluya, al menos, 4 funciones.

  ```
  // Devuelve el largo de una cadena sin contar el caracter de termino \0.
  size_t strlen(const char *s);

  // Compara dos cadenas.
  // Devuelve un entero menor, igual o mayor a cero si s1 es menor, igual
  // o mayor a s2.
  int strcmp(const char *s1, const char *s2);

  // Copia el string src a dest.
  // Devuelve un puntero al inicio de dest.
  char *strcpy(char *dest, char *src);

  // Devuelve un puntero a la primera ocurrencia de neegle en haystack.
  // NULL si no se encuentra.
  char *strstr(const char *haystack, const char *needle);

  ```

6. ¿Qué es una macro de C? Detalle las buenas prácticas para su definición. Ejemplifique.

  Las **macros** en C son porciones de codigo con nombre. Cuando son usadas
  se reemplazan con su código. Hay dos tipos: object-like y function-like macros.

  - Object-like: Son usadas para definir constantes. Ejemplo. ```#define PI 3.1415```.
  - Function-like: Son usadas con paréntesis y se parecen a funciones. Ejemplo: ```#define MAX(X, Y) (X) > (Y) ? (X) : (Y)```.    

  Como buenas practicas se tiene:
  - definirlas en mayúsculas con nombres claros y
  legibles.
  - Si son function-like, expresar las operaciones con paréntesis en los argumentos dado que las macros no los evalúan antes de expandirse. Ej:
  ```
  #define MUL(X, Y) X * Y
  int x = MUL(2+2, 3+3);
  // No da x = 4 * 6 = 26, sino que 2 + 2 * 3 + 3 = 5 + 9 = 14 debido al
  // Orden de las operaciones.
  // Se evita definiendo
  # MUL(X, Y) (X) * (Y)
  // Porque MUL(2+2, 3+3) = (2 + 2) * (3 + 3) = 4 * 6 = 24.
  ```

7. Describa el proceso de transformación de código fuente a un ejecutable. Precise las etapas y las tareas desarrolladas en cada una de ellas.

  1. **Precompilación**    
    1.1 Sustitucion y control de precompilacion     
      - Se reemplazan constantes mediante la directiva ```#define``` (ej: ```#define MAX 3```)    
      - Se incluyen archivos mediante ```#include``` (ej: ```#include <stdio.h>```).    
      - Se controla la compilacion mediante ```#if, #else, #endif, #ifndef, #ifdef``` permitiendo agregar o quitar codigo según condiciones.
    1.2 Expansión de macros: Se reeplaza codigo por el simbolo. ej: ```#define MIN(X, y) ((X) < (Y)) ? (X) : (Y)```
  2. **Compilación**: Su finalidad es generar codigo objeto.    
    2.1 Parsing: Arma un arbol sintactico que se recorre de arriba hacia abajo mediante descenso recursivo.   
    2.2 Translation: Convierte los simbolos a instrucciones de ensamblador.   
    2.3 Assembler: Convierte las instrucciones a codigo objeto.
  3. **Linking**: Combina objetos y resuelve simbolos faltantes. Hay dos tipos:
    - Estatico: Todos los simbolos faltantes se agregan  en el mismo ejecutable.
    - Dinamico: Resuelve los simbolos mediante librerias en tiempo de ejecución.

8. Indique la salida de
  ```
  class A { A(){cout << “A()” << endl;}
  class B : public A { B(){cout << “B()” << endl;}
  int main () { B b; return 0;}
  ```
  La salida es
  ```
  A()
  B()
  ```

9. Implemente una función C++ denominada Sacar que reciba dos listas de elementos y devuelva una nueva lista con los elementos de la primera que no están en la segunda: ```std::list<T> Sacar(std::list<T> a,std::list<T> b);```   
  En archivo 09.cpp

10. Escriba un programa que reciba por línea de comandos un Puerto y una IP. El programa debe
establecer una unica conexión, quedar en escucha e imprimir en stdout todo lo recibido. Al recibir
el texto ‘FINAL’ debe finalizar el programa sin imprimir dicho texto.
  En archivo 10.c
