#include <string.h>
#include <stdio.h>
#include "socket0.h"

// Escriba un programa que reciba por línea de comandos un Puerto y una IP.
// El programa debe establecer una unica conexión, quedar en escucha e imprimir
// en stdout todo lo recibido. Al recibir el texto ‘FINAL’ debe finalizar
// el programa sin imprimir dicho texto.

#define HOST "localhost"
#define PORT "5555"

int main() {
  socket0_t socket;
  socket0_client_init(&socket, HOST, PORT);

  socket0_print_until(&socket, stdout, "FINAL");

  socket0_close(&socket);
  return 0;
}
