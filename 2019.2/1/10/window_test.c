#include <criterion/criterion.h>
#include "window.h"

Test(window_test, should_have_size) {
  window_t window;
  int size = 3;

  window_init(&window, size);

  cr_expect_eq(size, window_size(&window));
  window_destroy(&window);
}

Test(window_test, should_initially_be_empty) {
  window_t window;
  int size = 1;
  window_init(&window, size);

  char empty[] = { '\0' };

  cr_assert(window_contains(&window, empty));
  window_destroy(&window);
}

Test(window_test, should_push_a_character) {
  window_t window;
  int size = 3;
  window_init(&window, size);
  char new_char = 'X';

  window_push(&window, new_char);

  char expected_window[] = { 0x00, 0x00, 'X' };
  cr_assert(window_contains(&window, expected_window));
  window_destroy(&window);
}

Test(window_test, should_indicate_when_ends_with_a_string) {
  window_t window;
  int size = 6;
  window_init(&window, size);

  window_push(&window, 'X');
  window_push(&window, 'F');
  window_push(&window, 'I');
  window_push(&window, 'N');
  window_push(&window, 'A');
  window_push(&window, 'L');

  cr_assert(window_ends_with(&window, "FINAL"));
  window_destroy(&window);
}

Test(window_test, should_return_first_element) {
  window_t window;
  int size = 6;
  window_init(&window, size);

  window_push(&window, 'X');
  window_push(&window, 'F');
  window_push(&window, 'I');
  window_push(&window, 'N');
  window_push(&window, 'A');
  window_push(&window, 'L');

  cr_expect_eq('X', window_first(&window));
  window_destroy(&window);
}

Test(window_test, should_move_elements_When_window_is_too_small) {
  window_t window;
  int size = 3;
  window_init(&window, size);

  window_push(&window, 'X');
  window_push(&window, 'F');
  window_push(&window, 'I');
  window_push(&window, 'N');
  window_push(&window, 'A');
  window_push(&window, 'L');

  cr_assert(window_contains(&window, "NAL"));
  window_destroy(&window);
}
