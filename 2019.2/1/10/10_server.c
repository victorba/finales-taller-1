#include <string.h>
#include <stdio.h>
#include "socket.h"

#define SERVICE "5555"

int main() {
  socket_t socket;
  socket_t peer;
  socket_server_init(&socket, SERVICE);
  socket_listen(&socket, 1);
  socket_accept(&socket, &peer);

  char text[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZFINALXXX";
  socket_send_all(&peer, text, sizeof(text));

  socket_shutdown(&peer, SOCKET_SHUT_RDWR);
  socket_shutdown(&socket, SOCKET_SHUT_RDWR);
  socket_close(&peer);
  socket_close(&socket);
  return 0;
}
