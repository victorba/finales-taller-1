#ifndef __SOCKET0_H__
#define __SOCKET0_H__

#define _POSIX_C_SOURCE 200112L

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include "window.h"

#define ERROR_CODE -1
#define SUCCESS_CODE 0

typedef struct __socket0_t {
  int sockfd;
} socket0_t;

int socket0_server_init(socket0_t *self, char service[]);
int socket0_client_init(socket0_t *self, char node[], char service[]);
int socket0_listen(socket0_t *self, int backlog);
int socket0_accept(socket0_t *self, socket0_t *accepted_socket);
int socket0_close(socket0_t *self);
int socket0_print_until(socket0_t *self, FILE *file, char endstring[]);

#endif
