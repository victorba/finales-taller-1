#include "window.h"
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

void window_init(window_t *self, int size) {
  self->size = size;
  self->buffer = malloc(size);
  memset(self->buffer, 0, self->size);
}

void window_destroy(window_t *self) {
  free(self->buffer);
}

int window_size(window_t *self) {
  return self->size;
}

bool window_contains(window_t *self, char string[]) {
  return memcmp(self->buffer, string, self->size) == 0;
}

void window_push(window_t *self, char new_char) {
  memcpy(self->buffer, self->buffer + 1, self->size - 1);
  self->buffer[self->size - 1] = new_char;
}

bool window_ends_with(window_t *self, char string[]) {
  return memcmp(
    &self->buffer[self->size - strlen(string)],
    string,
    strlen(string)
  ) == 0;
}

char window_first(window_t *self) {
  return self->buffer[0];
}
