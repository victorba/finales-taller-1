#ifndef __WINDOW_H__
#define __WINDOW_H__

#include <stdbool.h>

typedef struct __window_t {
  int size;
  char *buffer;
} window_t;

void window_init(window_t *self, int size);

void window_destroy(window_t *self);

int window_size(window_t *self);

// pre: string must be longer than window size
bool window_contains(window_t *self, char string[]);

void window_push(window_t *self, char new_char);

bool window_ends_with(window_t *self, char string[]);

char window_first(window_t *self);

#endif
