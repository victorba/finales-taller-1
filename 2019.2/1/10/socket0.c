#include "socket0.h"

int socket0_server_init(socket0_t *self, char service[]) {
  struct addrinfo hints;
  struct addrinfo *res;
  struct addrinfo *ptr;
  int r;
  int sockfd;
  bool are_we_bound = false;
  int val = 1;

  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE;

  if ((r = getaddrinfo(NULL, service, &hints, &res)) < 0) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(r));
    return ERROR_CODE;
  }

  for (ptr = res; ptr != NULL && !are_we_bound; ptr = ptr->ai_next) {
    sockfd = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
    if (sockfd != -1) {
      r = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
      if (r != -1) {
        if ((r = bind(sockfd, ptr->ai_addr, ptr->ai_addrlen)) != -1) {
          are_we_bound = true;
        }
      }
    }
  }

  freeaddrinfo(res);

  if (!are_we_bound) {
    fprintf(stderr, "Socket could not bind\n");
    return ERROR_CODE;
  }

  self->sockfd = sockfd;
  return SUCCESS_CODE;
}

int socket0_client_init(socket0_t *self, char node[], char service[]) {
  struct addrinfo hints;
  struct addrinfo *res;
  struct addrinfo *ptr;
  int r;
  int sockfd;
  bool are_we_connected = false;

  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = 0;

  if ((r = getaddrinfo(node, service, &hints, &res)) < 0) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(r));
    return ERROR_CODE;
  }

  for (ptr = res; ptr != NULL && !are_we_connected; ptr = ptr->ai_next) {
    sockfd = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
    if (sockfd != -1) {
      if ((r = connect(sockfd, ptr->ai_addr, ptr->ai_addrlen)) != -1) {
        are_we_connected = true;
      }
    }
  }

  freeaddrinfo(res);

  if (!are_we_connected) {
    fprintf(stderr, "socket: impossible to connect\n");
    return ERROR_CODE;
  }

  self->sockfd = sockfd;
  return SUCCESS_CODE;
}

int socket0_listen(socket0_t *self, int backlog) {
  int r;
  if ((r = listen(self->sockfd, backlog)) == -1) {
    fprintf(stderr, "Error: listen %s\n", strerror(errno));
    return ERROR_CODE;
  }
  return SUCCESS_CODE;
}

int socket0_accept(socket0_t *self, socket0_t *accepted_socket) {
  int sockfd;
  if ((sockfd = accept(self->sockfd, NULL, NULL)) == -1) {
    fprintf(stderr, "Error: accept %s\n", strerror(errno));
    return ERROR_CODE;
  }
  return SUCCESS_CODE;
}

int socket0_close(socket0_t *self) {
  int r;
  if ((r = close(self->sockfd)) == -1) {
    fprintf(stderr, "Error: close %s\n", strerror(errno));
    return ERROR_CODE;
  }
  return SUCCESS_CODE;
}

int socket0_print_until(socket0_t *self, FILE *file, char endstring[]) {
  int r;
  char buffer;
  window_t window;
  bool endstring_found = false;

  window_init(&window, strlen(endstring));

  while ((r = recv(self->sockfd, &buffer, sizeof(buffer), 0)) > 0 && !endstring_found) {
    window_push(&window, buffer);
    if (window_ends_with(&window, endstring)) {
      endstring_found = true;
    } else if (window_first(&window) != '\0') {
      char first = window_first(&window);
      fwrite(&first, sizeof(first), 1, file);
    }
  }

  window_destroy(&window);
  return SUCCESS_CODE;
}
