#include <list>
#include <iostream>

template <class T>
bool contains(std::list<T> list, T x) {
  for (T elem: list) {
    if (elem == x) {
      return true;
    }
  }
  return false;
}

template <class T>
std::list<T> sacar(std::list<T> a, std::list<T> b) {
  std::list<T> res;

  for (T elem: a) {
    if (!contains(b, elem)) {
      res.push_back(elem);
    }
  }

  return res;
}

int main() {
  std::list<int> a({1, 2, 3});
  std::list<int> b({3, 4, 5});

  std::list<int> res = sacar(a, b);

  for (int elem: res) {
    std::cout << elem << std::endl;
  }

  return 0;
}
