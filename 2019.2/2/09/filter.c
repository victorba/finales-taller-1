#include "filter.h"
#include <stdbool.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/types.h>


void file_filter(bool (*f) (uint16_t x), const char pathname[]) {
  FILE *file;
  long registers;
  uint16_t buffer;
  int offset = 0;
  long size;

  file = fopen(pathname, "r+");
  size = file_size(file);
  registers = file_size(file) / sizeof(buffer);

  for (long i = 0; i < registers; i++) {
    fseek(file, offset, SEEK_SET);
    fread(&buffer, sizeof(buffer), 1, file);
    buffer = ntohs(buffer);
    if (!f(buffer)) {
      file_cpy(
        file,
        offset, offset + sizeof(buffer),
        size - (offset + sizeof(buffer))
      );
    } else {
      offset += sizeof(buffer);
    }
  }

  truncate(pathname, offset);
  fclose(file);
}

void file_cpy(FILE *file, int dst_offset, int src_offset, int size) {
  char buffer;
  for (int i = 0; i < size; i++) {
    fseek(file, src_offset, SEEK_SET);
    fread(&buffer, sizeof(buffer), 1, file);
    fseek(file, dst_offset, SEEK_SET);
    fwrite(&buffer, sizeof(buffer), 1, file);
    src_offset += sizeof(buffer);
    dst_offset += sizeof(buffer);
  }
}

long file_size(FILE *file) {
  long current_pos = ftell(file);
  fseek(file, 0, SEEK_END);
  long size = ftell(file);
  fseek(file, current_pos, SEEK_SET);
  return size;
}
