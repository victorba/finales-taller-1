#include <criterion/criterion.h>
#include "filter.h"
#include <stdlib.h>
#include <stdio.h>

bool f(uint16_t x) {
  return x % 3 != 0;
}

Test(filter_test, should_filter) {
  system("cp ./input.dat ./input_filter.dat");

  file_filter(f, "./input_filter.dat");

  cr_expect_eq(0, system("diff ./input_filter.dat exp_output.dat"));
  system("rm ./input_filter.dat");
}

Test(filter_test, should_return_file_size) {
  FILE *file;
  file = fopen("./input.dat", "r+");

  long size = file_size(file);

  cr_expect_eq(12, size);
  fclose(file);
}

Test(filter_test, should_copy_file_from_src_to_dst_offset) {
  system("cp ./input.dat ./should_copy_file_from_src_to_dst_offset.dat");
  FILE *file;
  file = fopen("./should_copy_file_from_src_to_dst_offset.dat", "r+");

  file_cpy(file, 2, 4, 8);
  fflush(file);

  cr_expect_eq(
    0,
    system("diff should_copy_file_from_src_to_dst_offset.dat exp_output_file_cpy.dat")
  );
  system("rm ./should_copy_file_from_src_to_dst_offset.dat");
  fclose(file);
}
