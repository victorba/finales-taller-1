#ifndef __FILTER_H__
#define __FILTER_H__

#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>

void file_filter(bool (*f) (uint16_t x), const char pathname[]);

void file_cpy(FILE *file, int dst_offset, int src_offset, int size);

long file_size(FILE *file);

#endif
