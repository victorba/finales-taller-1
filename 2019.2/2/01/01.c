#define _POSIX_C_SOURCE 200112L

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>

#define PORT "5556"
#define BACKLOG 1
#define MAXBUFFER 256

void feed_buffer(char buffer[], int *i, char character) {
  printf("FEED BUFFER --> character = %c\n", character);
  buffer[*i] = character;
  (*i)++;
}

void add_number(int *total, char buffer[], int *i) {
  buffer[*i] = '\0';
  int number = atoi(buffer);
  *total += number;
  *i = 0;
  memset(buffer, 0, MAXBUFFER);
  printf("ADD NUMBER --> total = %d\n", *total);
}

void reset_all(char buffer[], int *i, int *total) {
  printf("RESETING");
  memset(buffer, 0, MAXBUFFER);
  *i = 0;
  *total = 0;
}

void stop_all(bool *condition) {
  printf("STOPPING");
  *condition = false;
}

int main() {
  int sockfd;
  int peerfd;
  struct addrinfo hints;
  struct addrinfo *res;
  struct addrinfo *ptr;
  bool are_we_bound = false;
  int val = 1;

  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE;

  getaddrinfo(NULL, PORT, &hints, &res);

  for (ptr = res; ptr != NULL && !are_we_bound; ptr = ptr->ai_next) {
    sockfd = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
    if (sockfd != -1) {
      setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
      bind(sockfd, ptr->ai_addr, ptr->ai_addrlen);
      are_we_bound = true;
    }
  }

  freeaddrinfo(res);

  if (!are_we_bound) {
    return -1;
  }

  listen(sockfd, BACKLOG);
  peerfd = accept(sockfd, NULL, NULL);

  // Empieza a recibir
  char character;
  char buffer[MAXBUFFER];
  int i = 0;
  int total = 0;
  bool keep_working = true;

  while (keep_working) {
    recv(peerfd, &character, sizeof(character), 0);

    if (character == '+') {
      add_number(&total, buffer, &i);
    } else if (character == '=' && i == 0) {
      stop_all(&keep_working);
    } else if (character == '=' && i > 0) {
      add_number(&total, buffer, &i);
      printf("total: %d\n", total);
      reset_all(buffer, &i, &total);
    } else {
      feed_buffer(buffer, &i, character);
    }
  }

  // Cierre
  printf("closing\n");
  close(peerfd);
  close(sockfd);

  return 0;
}
