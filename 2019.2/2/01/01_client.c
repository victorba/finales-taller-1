#include "./socket.h"

#define HOST "localhost"
#define PORT "5556"

int main() {
  socket_t socket;
  socket_client_init(&socket, HOST, PORT);

  char text[] = "1+10+222=2+20+555==XXXXXXX";
  socket_send_all(&socket, text, strlen(text));

  socket_shutdown(&socket, SOCKET_SHUT_RDWR);
  socket_close(&socket);
  return 0;
}
