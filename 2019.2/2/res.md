# Final 17 / 12 / 2019

1. Escriba un programa (desde la inicialización hasta la liberación de los recursos) que reciba paquetes de la forma nnn+nn+....+nnnn= (numeros separados por +, seguidos de =) e imprima el resultado de la suma de cada paquete por pantalla. Al recibir un paquete vacío (“=”) debe cerrarse ordenadamente. No considere errores.   
  *En 01.c*

2. Explique breve y concretamente qué es f: ```int (*f) (short *, char[4]);```    
  F es un puntero a una funcion que recibe por parametros a un puntero a un short (aka short int, entero de 2 bytes con signo) y un puntero a un array de chars de 4 elementos. *Ejemplo en 02.c*

3. Analice el siguiente código y determine lo que se imprime (valor de Pi)
  ```
  main()
  {
  int *Pi=1000;
  Pi++;
  printf(“Pi apunta a la dirección: %l”, (long)Pi);
  }
  ```
  En la primera linea se crea un puntero a entero Pi que apunta a la direccion 1000. Luego, se incrementa en un elemento que son 4 u 8 bytes dependiendo de la arquitectura y entonces se obtiene Pi = 1004 / 1008. Finalmente se imprime la direccion como long. *Ver 03.c*

4. ¿Qué es un functor? ¿Qué ventaja ofrece frente a una función convencional? Ejemplifique.   
  Un ***functor*** es un objeto que se comporta como funcion, es decir, responde al mensaje () el cual puede o no tener parametros. Frente a una funcion tradicional tiene la ventaja que puede tener estado y responder a mas mensajes.

5. ¿Cómo se logra que 2 threads accedan (lectura/escritura) a un mismo recurso compartido sin que se generen problemas de consistencia? Ejemplifique.   
  Se puede mediante exclusion mutua mediante un mutex de tal forma que al obtener un thread un recurso, este toma un lock y ningun otro thread puede entrar al recurso mientras no lo suelte.  Una forma mas avanzada es utiliar a un *monitor* que es un objeto que se construye con un mutex y por lo tanto posee la capacidad de excluir threads. *ver 05.cpp*

6. Describa el concepto de loop de eventos (events loop) utilizado en programación orientada a eventos y, en particular, en entornos de interfaz gráfica (GUIs)     
    *Ver cuaderno*

7. Considere la estructura struct ejemplo { int a; char b;}. ¿Es verdad que ```sizeof (ejemplo)=sizeof(a)+sizeof(b)```? Justifique.     
  Falso, segun la arquitectura e indicacion al compilador se puede hacer padding a cada elemento para obtener accesos mas rapidos a los elementos del struct.
  Por ejemplo en una arquitectura de 32 bits un struct como:
  ```
  struct Example {
    char a,
    char b
  };
  ```
  que se podria pensar que debera ocupar 2 bytes (1 byte por elemento), en realidad por padding, agregar mas bytes para llenar un determinado tamaño (en el ejemplo 32 bits), ocuparia 8 bytes (4 bytes por elemento). Mediante macros se puede forzar al compilador a que no realice padding.

8. ¿En qué consiste el patrón de diseño RAII? Ejemplifique.   
    RAII significa Resource Acquisition Is Initialization, y significa que al instanciarse el objeto se realiza la alocacion de recursos y al destruirse se liberan.
    Ejemplo:
    ```
    class Counter {
    private:
      int *n;
    public:
      Counter(): n(new int) {}
      void inc() { (*n)++ }
      int value() { return *n; }
      ~Counter() { delete n }
    };

    int main(){
      Counter c;
      c.inc();
      std::cout << c.value() << std::endl;
    }
    ```
    En el ejemplo, al instanciar al contador se alocan su ```int n``` en el heap y al destruirse se libera automaticamente con lo cual disminuye el riesgo de leaks de memoria. Como limitacion tiene que la memoria alocada en el heap debe ser codeada porel programador en el destructor y por lo tanto es propensa a leaks de memoria.

9. Escribir un programa ISO C que procese el archivo de enteros de 2 bytes bigendian cuyo nombre es recibido como parámetro. El procesamiento consiste en eliminar los número múltiplos de 3, trabajando sobre el mismo archivo (sin archivos intermedios ni en memoria).
  *Ver carpeta 09*

10. Implemente una función C++ denominada DobleSiNo que reciba dos listas de elementos y devuelva una nueva lista duplicando los elementos de la primera que no están en la segunda: ```std::list<T> DobleSiNo(std::list<T> a,std::list<T> b);```
  *Ver carpeta 10"
