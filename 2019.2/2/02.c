#include <stdio.h>

int (*f) (short *, char[4]);

int add_all(short *x, char array[4]) {
  for (int i = 0; i < 4; i++) {
    array[i] += *x;
  }
  return 0;
}

int subs_all(short *x, char array[4]) {
  for (int i = 0; i < 4; i++) {
    array[i] -= *x;
  }
  return 0;
}

int main() {
  short x = 1;
  char text1[] = "hola";
  char text2[] = "hola";

  add_all(&x, text1);
  subs_all(&x, text2);

  printf("text1: %s\n", text1);
  printf("text2: %s\n", text2);

  return 0;
}
