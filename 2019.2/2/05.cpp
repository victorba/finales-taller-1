
#include <thread>
#include <mutex>
#include <iostream>

class Counter {
private:
  std::mutex m;
  int n;

public:
  Counter(): n(0) {};

  void inc() {
    std::lock_guard<std::mutex> l(this->m);
    this->n++;
  }

  int value() {
    std::lock_guard<std::mutex> l(this->m);
    return this->n;
  }

};

void inc_a_lot(Counter *c) {
  for (int i = 0; i < 100; i++) {
    c->inc();
  }
}

int main() {
  Counter c;

  std::thread t1(inc_a_lot, &c);
  std::thread t2(inc_a_lot, &c);

  t1.join();
  t2.join();

  std::cout << c.value() << std::endl;

  return 0;
}

// Si Counter no excluyera mutuamente a los threads, entonces al
// ser el incremente no atomico se produciria un race condition (es decir
// al intentar los dos threads acceder al Counter a la vez se podria
// producir distintos resultados segun el schedule resultante
// el cual podria producir resultados no deseados)
// lo cual la suma total seria menor a 200.
