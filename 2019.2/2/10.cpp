#include <list>
#include <iostream>

template <class T>
bool list_contains(std::list<T> l, T searched_elem) {
  for (T elem: l) {
    if (searched_elem == elem) {
      return true;
    }
  }
  return false;
}

template <class T>
std::list<T> DobleSiNo(std::list<T> a, std::list<T> b) {
  std::list<T> r;
  for (T a_elem: a) {
    if (!list_contains(b, a_elem)) {
      r.push_back(a_elem);
      r.push_back(a_elem);
    }
  }
  return r;
}

int main() {
  std::list<int> a({ 1, 2, 3, 4, 5 });
  std::list<int> b({ 3, 5 });

  std::list<int> res = DobleSiNo(a, b);

  for (int e: res) {
    std::cout << e << std::endl;
  }
  return 0;
}
