// Indique la salida del siguiente programa:
//
//  A()
//  B()
// ~B()
// ~A()

#include <iostream>

using namespace std;

class A {
public:
    A() { cout<< "A()" << endl;}
    ~A(){ cout<< "~A()" << endl;}
};

class B:public A {
public:
    B() { cout<< "B()" << endl;}
    ~B(){ cout<< "~B()" << endl;}
};

int main() { B b; return 0; }
