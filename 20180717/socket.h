#ifndef __SOCKET_H__
#define __SOCKET_H__

#define _POSIX_C_SOURCE 200112L

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>

#define ERROR_CODE -1
#define SUCCESS_CODE 0

typedef struct _socket_t {
    int sockfd;
} socket_t;

// Initializes a client socket
// Node and service identify an internet host and service.
// Returns -1 on error and 0 on success.
int socket_client_init(
    socket_t *skt,
    const char node[],
    const char service[]);

// Initializes a server socket.
// Returns -1 on error and 0 on success.
int socket_server_init(
    socket_t *skt,
    const char service[]);

// Closes a socket.
int socket_close(socket_t *skt);

// Marks the socket referred to by skt as a passive socket.
// backlog defines the maximum length to which the queue of pending
// connections for skt may grow.
// Returns -1 on error and 0 on success.
int socket_listen(socket_t *skt, int backlog);

// Accepts a new socket
// Returns -1 on error and 0 on success.
int socket_accept(socket_t *skt, socket_t *accepted_skt);

// Sends all the content of buffer.
// Returns the bytes sent or -1 on error.
int socket_send_all(socket_t *skt, const char buf[], unsigned int size);

// Recieves a content in buf.
// Returns the bytes received or -1 on error.
int socket_recv_all(socket_t *skt, char buf[], unsigned int size);

// Causes all or part of a full-duplex connection on the socket associated with
// skt to be shut down.
// how can be SHUT_RD, SHUT_WR, SHUT_RDWD.
// Return 0 on success or -1 on error.
int socket_shutdown(socket_t *skt, int how);

// Transmits a message to another socket.
// Returns the number of bytes receives, -1 on error, 0 if the connection
// was closed or if received 0 bytes.
int socket_send(socket_t *skt, const void *buf, size_t len, int flags);

// Recieves messages from a socket.
// Returns the bytes sended or -1 on error.
int socket_recv(socket_t *skt, void *buf, size_t len, int flags);

#endif
