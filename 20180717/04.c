#include <stdio.h>

// Devuelve 'A' aprobado, 'F' reprobado.
char nota(float *x, unsigned v[3]) {
    unsigned int total = 0;
    for (int i = 0; i < 3; i++) {
        total += v[i];
    }

    float promedio = ((float)total / 3) + (*x);

    if (promedio > 4) {
        return 'A';
    } else {
        return 'F';
    }
}

int main() {
    char (*f)(float *, unsigned [3]);
    f = &nota;

    unsigned int v[3] = {5, 6, 7};
    float x = 1;

    printf("nota: %c\n", f(&x, v));
    return 0;
}
