// test

#include <iostream>

class Person {
public:
    // Declaracion
    static int age;
};

// Definicion
int Person::age = 25;

int global = 0;
static int static_global = 1;

int inc() {
    static int static_local = 0;
    static_local++;
    return static_local;
}

int dec() {
    int local = 9;
    local--;
    return local;
}

int main() {

    inc();
    inc();
    inc();

    std::cout << "static_global: " << static_global
              << "\nglobal: " << global
              << "\nstatic_local: " << inc()
              << "\nlocal: " << dec()
              << "\nperson static age: " << Person::age
              << std::endl;

    return 0;
}
