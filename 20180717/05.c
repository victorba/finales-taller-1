//Escribir un programa ISO C que procese el archivo "nros1byte.dat"
// sobre si mismo, eliminando los bytes multiplos de 6.

#include <stdio.h>
#include <errno.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>

#define PATHNAME "nros1byte-01.dat"

int main() {
    FILE *file;
    uint8_t n;
    int size;
    int move_spaces = 0;


    if ((file = fopen(PATHNAME, "r+")) == NULL) {
        perror("Error fopen");
        return -1;
    }

    while (fread(&n, sizeof(n), 1, file) == 1) {
        if (n % 6 == 0) {
            move_spaces++;
        } else if (move_spaces != 0) {
            fseek(file, -(move_spaces + 1), SEEK_CUR);
            fwrite(&n, sizeof(n), 1, file);
            fseek(file, move_spaces, SEEK_CUR);
        }
    }

    size = ftell(file);

    // Antes de hacer ftruncate es necesario asegurarse de que todos los
    // datos hayan sido escritos al archivo.
    fflush(file);
    ftruncate(fileno(file), size - move_spaces);

    fclose(file);

    return 0;
}
