3) Que es una macro en C?

Es un fragmento de codigo al que se le ha dado nombre y que luego es reemplazado por
el precompilador por el codigo.

Uso tipicos son para definir constantes como ej. en el que se reemplaza MAX
por el valor 100 en cada una de sus intancias.

#define MAX 100
int main() {
    int x = 1;
    if (x > MAX) {
        return 0;
    } else {
        return -1;
    }
}

Tambien puede ser usado para escribir codigo mas complejo similar a funciones
como

#define MIN(X, Y) (X < Y ? X : y)

Otros casos son para indicarle al precompilador que debe incluir otros archivos
como #include <stdio.h>. 
