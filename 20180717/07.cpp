// Implemente una funcion C++ denominada SinSegunda que reciba dos listas de elementos
// y devuelva una nueva lista con los elementos de la primera que no estan en la segunda.
// std::list<T> SinSegunda(std::list<T> a, std::list<T> b);

#include <iostream>
#include <list>

template<class T>
std::list<T> SinSegunda(std::list<T> a, std::list<T> b);

// Indica si la lista contiene al elemento elem.
template<class T>
bool contiene(const std::list<T> &l, const T &elem);

int main() {
    std::list<int> a({1, 2, 3, 4});
    std::list<int> b({1, 3});

    std::list<int> r;
    r = SinSegunda(a, b);

    std::cout << "size: " << r.size() << std::endl;
    for (const int &e: r) {
        std::cout << e << std::endl;
    }

    return 0;
}

template<class T>
std::list<T> SinSegunda(std::list<T> a, std::list<T> b) {
    std::list<T> r;

    for (const T &elem: a) {
        if (!contiene(b, elem)) {
            r.push_back(elem);
        }
    }

    return r;
}

template<class T>
bool contiene(const std::list<T> &l, const T &elem) {
    for (const T &e: l) {
        if (e == elem) {
            return true;
        }
    }
    return false;
}
