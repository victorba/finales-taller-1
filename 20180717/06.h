// Escriba el .H de una biblioteca de funciones ISO C para cadenas de caracteres.
// Incluya al menos 4 funciones.

// <string.h>

// Compara las cadenas dest y src hasta el n-esimo caracter.
// Devuelve 0 si son iguales, menor a cero si los caracteres de dest
// son menores a src o mayor a cero en caso contrario.
int strncmp0(char *dest, char *src, ssize_t n);

// Copia a string de src a dest incluyendo el caracter '\n' hasta n
// caracteres.
// Devuelve un puntero del string de destino.
int strncpy0(char *dest, char *src, ssize_t n);

// Devuelve la longitud de la cadena s sin contar el caracter '\n'.
int strlen0(char *s);

// Devuelve un puntero a una copia del string s en el heap.
// Debe ser liberado mediante free()
char *strdup(char *s);
