// Escriba un programa que reciba por linea de comandos un Puerto y una IP.
// El programa debe aceptar una unica conexion e imprimir en stdout todo lo
// recibido. Al recibir el texto FIN debe finalizar el programa sin imprimir
// dicho texto.

#include "socket.h"
#include "buffered_input_stream.h"
#include <string.h>
#include <stdbool.h>

#define NODE "localhost"
#define SERVICE "4747"
#define MAX_BIS_SIZE 4
#define END_TEXT "FIN"

void test();

int main(int argc, char *argv[]) {
    if (argc > 1) {
        test();
        return 0;
    }

    socket_t cli;
    BIS_t bis;
    char c;
    bool seguir_recibiendo = true;
    char buffer[4];

    memset(buffer, '\0', 4);
    socket_client_init(&cli, NODE, SERVICE);
    BIS_init(&bis, &cli, MAX_BIS_SIZE);

    while (seguir_recibiendo) {
        BIS_read(&bis, &c);
        memcpy(buffer, &buffer[1], 2);
        buffer[2] = c;

        if (strstr(buffer, END_TEXT) != NULL) {
            seguir_recibiendo = false;
        } else if (buffer[0] != '\0') {
            putchar(buffer[0]);
        }
    }

    socket_close(&cli);

    return 0;
}

void test() {
    socket_t sv;
    socket_t peer;
    char buf[] = "asdfgqwertFIN";

    socket_server_init(&sv, SERVICE);
    socket_listen(&sv, 100);
    socket_accept(&sv, &peer);

    socket_send_all(&peer, buf,strlen(buf));

    socket_close(&peer);
    socket_close(&sv);
}
